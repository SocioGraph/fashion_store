import Images from '../assets/images';

const {
  p1,
  p2,
  p3,
  p4,
} = Images

export const ProductCardData = [
  { id: 1, productImage: p1, productId: 1231 },
  { id: 2, productImage: p2, productId: 1232 },
  { id: 3, productImage: p3, productId: 1233 },
  { id: 4, productImage: p4, productId: 1234 },
  { id: 5, productImage: p4, productId: 1235 },
  { id: 6, productImage: p3, productId: 1236 },
  { id: 7, productImage: p2, productId: 1237 },
  { id: 8, productImage: p1, productId: 1238 },
  { id: 9, productImage: p1, productId: 1231 },
  { id: 10, productImage: p2, productId: 1232 },
  { id: 11, productImage: p3, productId: 1233 },
  { id: 12, productImage: p4, productId: 1234 },
  { id: 13, productImage: p4, productId: 1235 },
  { id: 14, productImage: p3, productId: 1236 },
  { id: 15, productImage: p2, productId: 1237 },
  { id: 16, productImage: p1, productId: 1238 },
  { id: 17, productImage: p1, productId: 1231 },
  { id: 18, productImage: p2, productId: 1232 },
  { id: 19, productImage: p3, productId: 1233 },
  { id: 20, productImage: p4, productId: 1234 },
]


export const PriceRangeSelector = [
  { id: 0, title: "Low To High", selected: false, key: true },
  { id: 1, title: "High to Low", selected: false, key: false }
]
export const maleColourSelector = [
  { id: 0, title: "Blue", selected: false, key: "Blue" },
  { id: 1, title: "Grey", selected: false, key: "Grey" },
  { id: 2, title: "Brown", selected: false, key: "Brown" },
  { id: 3, title: "White", selected: false, key: "White" },
  { id: 4, title: "Maroon", selected: false, key: "Maroon" },
  { id: 4, title: "Black", selected: false, key: "Black" },
]

export const feMaleColourSelector = [
  { id: 0, title: "Yellow", selected: false, key: "Yellow" },
  { id: 1, title: "White", selected: false, key: "White" },
  { id: 2, title: "Black", selected: false, key: "Black" },
  { id: 3, title: "Blue", selected: false, key: "Blue" },
  { id: 4, title: "Green", selected: false, key: "Green" },
  { id: 4, title: "OLIVE", selected: false, key: "OLIVE" },
]

export const maleBrandsSelector = [
  { id: 0, title: "Van Heusen", selected: false, key: "Van Heusen" },
  { id: 1, title: "Raymond", selected: false, key: "Raymond" },
  { id: 2, title: "Reebok", selected: false, key: "Reebok" },
  { id: 3, title: "US Polo Association", selected: false, key: "US Polo Association" },
  { id: 4, title: "Sparx", selected: false, key: "Sparx" },
  { id: 4, title: "Peter England", selected: false, key: "Peter England" },
  { id: 4, title: "BATA", selected: false, key: "BATA" },
]
export const feMaleBrandsSelector = [
  { id: 0, title: "Symbol", selected: false, key: "Symbol" },
  { id: 0, title: "Janasya", selected: false, key: "Janasya" },
  { id: 0, title: "AKA CHIC", selected: false, key: "AKA CHIC" },
  { id: 0, title: "Hidesign", selected: false, key: "Hidesign" },
  { id: 0, title: "Sparx", selected: false, key: "Sparx" },
  { id: 0, title: "Miss Olive", selected: false, key: "Miss Olive" },
  { id: 0, title: "Campus Sutra", selected: false, key: "Campus Sutra" },
]

export const maleSubCategorySelector = [
  { id: 0, title: "T-shirt", selected: false, key: "T-shirt" },
  { id: 1, title: "Jackets", selected: false, key: "Jackets" },
  { id: 2, title: "formal shirts", selected: false, key: "formal shirts" },
  { id: 3, title: "casual trousers", selected: false, key: "casual trousers" },
  { id: 4, title: "casual shirts", selected: false, key: "casual shirts" },
  { id: 5, title: "shorts", selected: false, key: "shorts" },
  { id: 5, title: "sports shoes", selected: false, key: "sports shoes" },
]

export const feMaleSubCategorySelector = [
  { id: 0, title: "Dresses", selected: false, key: "Dresses" },
  { id: 5, title: "kurtas and suits", selected: false, key: "kurtas and suits" },
  { id: 5, title: "Sweatshirts", selected: false, key: "Sweatshirts" },
  { id: 5, title: "Waistcoats", selected: false, key: "Waistcoats" },
  { id: 5, title: "Leggings", selected: false, key: "Leggings" },
  { id: 0, title: "Blazers", selected: false, key: "Blazers" },
  { id: 0, title: "tops", selected: false, key: "tops" },
]

export const sizeSelector = [
  { id: 0, title: "L", selected: false, key: "L" },
  { id: 1, title: "M", selected: false, key: "M" },
  { id: 2, title: "XL", selected: false, key: "XL" },
  { id: 3, title: "XXL", selected: false, key: "XXL" },
  { id: 4, title: "XXXL", selected: false, key: "XXXL" },
]

export const genderSelector = [
  { id: 0, title: "male", selected: false, key: "male" },
  { id: 1, title: "female", selected: false, key: "female" },
]

export const fitnessSelector = [
  {
    id: 0,
    title: "Perfect fit",
    selected: false,
    key: "Perfect fit"
  },
  {
    id: 1,
    title: "Fit",
    selected: false,
    key: "Fit"
  },
  {
    id: 2,
    title: "Loose",
    selected: false,
    key: "Loose"
  },
  {
    id: 3,
    title: "Long",
    selected: false,
    key: "Long"
  },
]

export const productCategoryList = [
  { id: 1, title: "Shirt Fit", },
  { id: 2, title: "Fabric", },
  { id: 3, title: "Sleeve", },
  { id: 4, title: "Cuff", },
  { id: 5, title: "Collar", },
  { id: 6, title: "Placket", },
  { id: 7, title: "Back Details", },
  { id: 8, title: "Pocket", },
  { id: 9, title: "Style", },
  { id: 10, title: "Flap", },
  { id: 11, title: "Patch", },
  { id: 12, title: "Contrast", },
  { id: 13, title: "Thread", },
  { id: 14, title: "Contrast Fabric", },
  { id: 15, title: "Button", },
  { id: 16, title: "Bottom Cut", },
  { id: 17, title: "Embroidery", }
]

export const getMaleMandatoryMeasurements = {
  'Top Wear': [
    {
      id: 1,
      name: 'chest',
      title: 'Chest',
      subTitle: '',
      measurement: '6-8 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'waist',
      title: 'waist',
      subTitle: 'Chest',
      measurement: '6-8 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'hip',
      title: 'hip',
      subTitle: 'Chest',
      measurement: '6-8 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'sleeve_length',
      title: 'Sleeve Length',
      subTitle: '',
      measurement: '+/- 2cm',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  "Bottom Wear": [
    {
      id: 1,
      name: 'waist',
      title: 'Waist',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'hip',
      title: 'hip',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'thigh',
      title: 'Thigh',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'front_length',
      title: 'Front length',
      subTitle: '',
      measurement: '+/- 4cm',
      required: false,
      isValidate: false,
      value: 0,
      description: 'top of waist to Bottom hmem'
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  'Normal': [
    {
      id: 1,
      name: 'chest',
      title: 'Chest',
      subTitle: '',
      measurement: '6-8 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'waist',
      title: 'waist',
      subTitle: 'Chest',
      measurement: '6-8 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'hip',
      title: 'hip',
      subTitle: 'Chest',
      measurement: '6-8 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'sleeve_length',
      title: 'Sleeve Length',
      subTitle: '',
      measurement: '+/- 2cm',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
}

export const getFeMaleMandatoryMeasurements = {
  'Top Wear': [
    {
      id: 1,
      name: 'bust',
      title: 'Bust',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'waist',
      title: 'Waist',
      subTitle: 'Chest',
      measurement: '4-6 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'bottom_hem',
      title: 'Bottom Hem',
      subTitle: '',
      measurement: '+/- 4cm',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 5,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  "Bottom Wear": [
    {
      id: 1,
      name: 'waist',
      title: 'Waist',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'hip',
      title: 'Hip',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'thigh',
      title: 'Thigh',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'front_length',
      title: 'Front Length',
      subTitle: '',
      measurement: '+/- 4cm',
      required: false,
      isValidate: false,
      value: 0,
      description: 'top of waist to Bottom hem'
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  'Normal': [
    {
      id: 1,
      name: 'bust',
      title: 'Bust',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'waist',
      title: 'Waist',
      subTitle: 'Chest',
      measurement: '4-6 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'bottom_hem',
      title: 'Bottom Hem',
      subTitle: '',
      measurement: '+/- 4cm',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 5,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
}

export const getMandatoryMeasurements = {
  'Mens Shirt': [
    {
      id: 1,
      name: 'chest',
      title: 'Chest',
      subTitle: '',
      measurement: '6-8 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'waist',
      title: 'waist',
      subTitle: 'Chest',
      measurement: '6-8 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'hip',
      title: 'hip',
      subTitle: 'Chest',
      measurement: '6-8 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'sleeve_length',
      title: 'Sleeve Length',
      subTitle: '',
      measurement: '+/- 2cm',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  'Mens Trouser': [
    {
      id: 1,
      name: 'waist',
      title: 'Wearing Waist',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'hip',
      title: 'hip',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'thigh',
      title: 'Thigh',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'front_length',
      title: 'Front length',
      subTitle: '',
      measurement: '+/- 4cm',
      required: false,
      isValidate: false,
      value: 0,
      description: 'top of waist to Bottom hmem'
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  'Mens Blazer': [
    {
      id: 1,
      name: 'chest',
      title: 'Chest',
      subTitle: '',
      measurement: '8-10 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'waist',
      title: 'Waist',
      subTitle: '',
      measurement: '8-10 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'hip',
      title: 'Hip',
      subTitle: '',
      measurement: '8-10 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'sleeve_length',
      title: 'Sleeve Length',
      subTitle: '',
      measurement: '+/- 2cm',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  'Mens Polo': [
    {
      id: 1,
      name: 'chest',
      title: 'Chest',
      subTitle: '',
      measurement: '4-6 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'waist',
      title: 'Waist',
      subTitle: 'Chest',
      measurement: '4-6 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'hip',
      title: 'Hip',
      subTitle: 'Chest',
      measurement: '4-6 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'sleeve_length',
      title: 'Sleeve Length',
      subTitle: '',
      measurement: '+/- 4cm',
      required: false,
      isValidate: false,
      value: 0,
      description: 'top of waist to Bottom hmem'
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  'Mens T-shirt': [
    {
      id: 1,
      name: 'chest',
      title: 'Chest',
      subTitle: '',
      measurement: '4-6 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'waist',
      title: 'Waist',
      subTitle: 'Chest',
      measurement: '4-6 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'hip',
      title: 'Hip',
      subTitle: 'Chest',
      measurement: '4-6 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'sleeve_length',
      title: 'Sleeve Length',
      subTitle: '',
      measurement: '+/- 4cm',
      required: false,
      isValidate: false,
      value: 0,
      description: 'top of waist to Bottom hmem'
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  'Womens Shirt': [
    {
      id: 1,
      name: 'bust',
      title: 'Bust',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'waist',
      title: 'Waist',
      subTitle: 'Chest',
      measurement: '4-6 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'bottom_hem',
      title: 'Bottom Hem',
      subTitle: '',
      measurement: '+/- 4cm',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 5,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  'Womens Trouser': [
    {
      id: 1,
      name: 'wearing_waist',
      title: 'Wearing Waist',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'hip',
      title: 'Hip',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'thigh',
      title: 'Thigh',
      subTitle: '',
      measurement: '2-4 cm',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'Front length',
      title: 'Bottom Hem',
      subTitle: '',
      measurement: '+/- 4cm',
      required: false,
      isValidate: false,
      value: 0,
      description: 'top of waist to Bottom hem'
    },
    {
      id: 5,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'weight',
      title: 'Weight',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ],
  'Normal': [
    {
      id: 1,
      name: 'height',
      title: 'Height',
      subTitle: '',
      measurement: '',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 2,
      name: 'collar',
      title: 'Collar',
      subTitle: '',
      measurement: '',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 3,
      name: 'shoulder',
      title: 'Shoulder',
      subTitle: '',
      measurement: '',
      required: true,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 4,
      name: 'waist',
      title: 'Waist',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 5,
      name: 'hip',
      title: 'Hip',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
    {
      id: 6,
      name: 'chest',
      title: 'Chest',
      subTitle: '',
      measurement: '',
      required: false,
      isValidate: false,
      value: 0,
      description: ''
    },
  ]
}