import { useEffect, useRef, useState } from 'react';
import Carousel, { consts } from 'react-elastic-carousel';
import Images from '../../assets/images';
import './productswiper.css';

const { SwipeArrow } = Images;

function ProductCardSwiper(props) {
    const [productCardList, setProductCardList] = useState([]);

    const carouselRef = useRef(null);

    useEffect(() => {
        setProductCardList(props.productCardListData)
    }, []);

    useEffect(() => {
        const setFromEvent = () => {
            props.onHandlePress()
        }
        window.addEventListener("mousemove", setFromEvent);
    }, []);

    const myArrow = ({ type, onClick, isEdge }) => {
        const pointer = type === consts.PREV ?
            <img src={SwipeArrow} className="product-card-swiper-arrow-left" onClick={onClick} />
            :
            <img src={SwipeArrow} className="product-card-swiper-arrow-right" onClick={onClick} />
        return (
            <div disabled={isEdge} className="display-flex align-items-center">
                {pointer}
            </div>
        )
    }

    return (
        <div>
            <Carousel
                ref={carouselRef}
                itemsToShow={1}
                renderArrow={myArrow}
                onChange={() =>
                    props.onHandlePress()
                }
                renderPagination={() => {
                    return (
                        <div className="display-row align-items-center height-0 width-0" />
                    )
                }}
            >
                {productCardList.map((item, index) => {
                    return (
                        <div className="product-card-swiper-main-view" key={index}>
                            {item.map((value, i) => {
                                return (
                                    <div key={i} className="product-card-main-view">
                                        <img src={value.productImage} className="product-swiper-card-image" />
                                        <div className="product-card-id-text" style={{
                                            marginBottom: i === 4 && i === 5 && i === 6 && i === 7 ? 0 : 15,
                                        }}>Style {value.productId}</div>
                                    </div>
                                )
                            })}
                        </div>
                    )
                })}
            </Carousel>
        </div>
    )
}

export default ProductCardSwiper;
