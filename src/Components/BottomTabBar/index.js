import React, { Component } from 'react';
import { withAlert } from 'react-alert';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import Images from '../../assets/images';
import history from '../../history';
import {
    apiActions, onAutoStopRecording, onConversationResponseAvailable, onError, onStreamingResultsAvailable, onTranscriptionAvailable
} from '../../server';
import MyKeyboard from '../MyKeyboard';
import './BottomTabBar.css';

const {
    IconArrowRight,
    IconArrowLeft,
    IconMicrophone,
    IconStopSquared,
    IconSend,
} = Images

export const Menu = (list, selected, onHandleClick) => {
    return (
        list.map((item, index) => {
            const title = Object.keys(item)[0];
            return <MenuItem onHandleClick={() => onHandleClick({ title: title, value: item[title] })} text={item[title]} key={index} selected={selected} />;
        })
    )
}

const MenuItem = ({ text, selected, onHandleClick }) => {
    return (
        <div className={`menu-item ${selected ? 'active' : ''}`} onClick={() => onHandleClick()}>{text}</div>
    )
};

const Arrow = ({ className }) => {
    return (
        className === 'arrow-prev' ?
            <div className={className} style={{
                backgroundColor: '#0a1498',
                padding: 3,
                paddingRight: 8,
                borderTopRightRadius: 5,
                borderBottomRightRadius: 5
            }}>
                <img
                    src={IconArrowLeft}
                    className="micro-img microphone-icon"
                    aria-hidden="true"
                />
            </div>
            :
            <div style={{
                backgroundColor: '#0a1498',
                padding: 3,
                paddingLeft: 8,
                borderTopLeftRadius: 5,
                borderBottomLeftRadius: 5
            }}>
                <img
                    src={IconArrowRight}
                    className="micro-img microphone-icon"
                    aria-hidden="true"
                />
            </div>
    );
};

const ArrowLeft = Arrow({ className: 'arrow-prev' });
const ArrowRight = Arrow({ className: 'arrow-next' });

const selected = 'item1';

class BottomTabBar extends Component {
    state = {
        selected,
        productName: '',
        searchKeywords: { sub_category: '', discount: 999 },
        isRecordVoice: false,
        isKeyboardVisible: false,
        isKeyboardLastEvent: '',
    };

    constructor(props) {
        super(props);
        this.menuItems = Menu(props.discountOptions, selected, props.handleClickDiscount);
    }

    onStartRecording = () => {
        this.setState({ isRecordVoice: true })
        apiActions.StreamingSpeech['onTranscriptionAvailable'] = onTranscriptionAvailable
        apiActions.StreamingSpeech['onAutoStopRecording'] = this.onAutoStopRecording
        apiActions.StreamingSpeech['onStreamingResultsAvailable'] = onStreamingResultsAvailable
        apiActions.StreamingSpeech['onConversationResponseAvailable'] = onConversationResponseAvailable
        apiActions.StreamingSpeech['onError'] = onError
        apiActions.StreamingSpeech.onStartVoiceRecording()
        this.setState({ productName: '' })
    }
    onAutoStopRecording = () => {
        this.setState({ isRecordVoice: false })
        this.setState({ productName: '' })
        document.getElementById("chat_spinner").style.display = "block";
    }

    onStopRecording = () => {
        this.setState({ isRecordVoice: false })
        document.getElementById("chat_spinner").style.display = "block";
        apiActions.StreamingSpeech.onStopVoiceRecording();
        // apiActions.StreamingSpeech.onAutoStopRecording();
        this.setState({ productName: '' })
    }

    onSelect = key => {
        this.setState({ selected: key });
    }

    onSubmitProduct = () => {
        this.props.onManageLoading(true)
        const dave_engagement_id = window.localStorage.getItem('dave_engagement_id');
        const dave_user_id = window.localStorage.getItem('dave_user_id');
        const { productName } = this.state;
        const finalData = document.getElementById("my_transcription").value || productName;
        const value = finalData.trim()
        this.state.searchKeywords.sub_category = value;
        this.setState({ searchKeywords: { sub_category: value, discount: 999 } })
        if (value) {
            window.localStorage.setItem("search_product_title", JSON.stringify(productName));
            apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST",
                {
                    "engagement_id": dave_engagement_id,
                    "refresh_cache": true,
                    "system_response": "sr_init",
                    "customer_response": this.state.searchKeywords.sub_category,
                },
                (response) => {
                    if (response) {
                        window.localStorage.setItem("search-keyword", JSON.stringify(response));
                        document.getElementById("my_transcription").value = '';
                        this.setState({ productName: '' })
                        document.getElementById("my_transcription").value = '';
                        if (response && response.data && response.data.thumbnails && response.data.thumbnails.length !== 0) {
                            const currentProduct = response?.data?.thumbnails[0]
                            window.localStorage.setItem("current_product", JSON.stringify(currentProduct));
                            response.data.thumbnails.shift();
                            window.localStorage.setItem("similar_products", JSON.stringify(response));
                            if (currentProduct && currentProduct.product_code) {
                                this.getRecommendedProducts(currentProduct?.product_code)
                            } else {
                                const value = {
                                    data: {
                                        thumbnails: []
                                    }
                                }
                                window.localStorage.setItem("recommended_products", JSON.stringify(value));
                                history.push({
                                    pathname: '/product',
                                });
                                this.props.onGetUpdatedData('true')
                            }
                        } else {
                            this.props.alert.error('No Data Found');
                            this.props.onManageLoading(false)
                        }

                    }
                }, (err) => {
                    this.props.alert.error('Something want to wrong, please try again');
                    this.props.onManageLoading(false)
                    document.getElementById("my_transcription").value = '';
                    this.setState({ productName: '' })
                }
            );
        } else {
            this.props.alert.error('Please enter valid keyword');
            this.props.onManageLoading(false)
        }
    }

    getRecommendedProducts = (product_code) => {
        const dave_engagement_id = window.localStorage.getItem('dave_engagement_id');
        const dave_user_id = window.localStorage.getItem('dave_user_id');
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
            "engagement_id": dave_engagement_id,
            "refresh_cache": true,
            "system_response": "sr_init",
            "customer_response": product_code,
            "customer_state": "cs_active_nudge_recommendations"
        },
            (resp) => {
                window.localStorage.setItem("recommended_products", JSON.stringify(resp));
                history.push({
                    pathname: '/product',
                });
                this.props.onGetUpdatedData('true')
                this.props.onManageLoading(false)

            },
            (err) => {
                this.props.onManageLoading(false)
                this.props.alert.error(err?.responseJSON?.error);
            }
        )
    }

    componentDidMount() {
        const listener = event => {
            if (event.code === "Enter" || event.code === "NumpadEnter") {
                event.preventDefault();
                // callMyFunction();
            }
        };
        document.addEventListener("keydown", listener);
        return () => {
            document.removeEventListener("keydown", listener);
        };
    }

    onChangeInput = (e) => {
        this.setState({ productName: e })
    }

    onCloseKeyboard = (value) => {
        this.setState({ isKeyboardVisible: false, isKeyboardLastEvent: value }, () => {
            if (this.state.productName) {
                this.onSubmitProduct()
            }
        })
        const timer = setTimeout(() => {
            this.setState({ isKeyboardLastEvent: '' })
        }, 1500);
        return () => clearTimeout(timer);
    }

    render() {

        const { productName, isKeyboardVisible, isKeyboardLastEvent } = this.state;
        const menu = this.menuItems;
        var discountOptionsLength = this.props.discountOptions.length;

        return (
            <>
                <div className="product-find-box" style={{ height: discountOptionsLength > 0 ? 215 : 160 }}>
                    <p className="product-find-box-title" > Can I help you with something? </p>
                    {discountOptionsLength > 0 &&
                        <ScrollMenu
                            translate={0}
                            data={menu}
                            arrowLeft={ArrowLeft}
                            arrowRight={ArrowRight}
                        // selected={selected}
                        />
                    }
                    <div className='display-row'>
                        <div className="input-border">
                            <div className='display-row justify-content-space-between'>
                                <input className="search-input border-none"
                                    placeholder="Type here...   Eg:Red T-Shirt or Product Id or summer wear"
                                    value={productName}
                                    type="text"
                                    id='my_transcription'
                                    onFocus={() => {
                                        if (isKeyboardLastEvent === '') {
                                            this.setState({ isKeyboardVisible: true })
                                        } else {
                                            document.getElementById('my_transcription').blur()
                                        }
                                    }}
                                    onChange={(e) => {
                                        this.setState({ productName: e.target.value })
                                    }}
                                />
                                <div className="send-bg-icon justify-content-center align-items-center display-flex">
                                    <img
                                        className="set-icon send-input-btn cursor-pointer"
                                        style={{ marginLeft: 15 }}
                                        src={IconSend}
                                        aria-hidden="true"
                                        onClick={() => {
                                            this.onSubmitProduct()
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        <div style={{ display: !this.state.isRecordVoice && 'none' }} className="bg-icon"
                            onClick={() => {
                                this.onStopRecording()
                            }}
                        >
                            <img
                                src={IconStopSquared}
                                className="micro-img microphone-icon"
                                aria-hidden="true"
                            />
                        </div>
                        <div style={{ display: this.state.isRecordVoice && 'none' }} className="bg-icon"
                            onClick={() => {
                                this.onStartRecording()
                            }}
                        >
                            <img
                                src={IconMicrophone}
                                className="micro-img microphone-icon"
                                aria-hidden="true"
                            />
                        </div>
                    </div>
                </div>
                <MyKeyboard
                    Screen={this.props.screen === 'Home' && this.props.isBarcodeScannerModel ? 'QRCode' : 'Normal'}
                    onCloseKeyboard={this.onCloseKeyboard}
                    isKeyboardVisible={isKeyboardVisible}
                    value={productName}
                    onChange={(e) => this.onChangeInput(e)}
                    onClearValue={() => { this.setState({ productName: '' }, () => { this.onCloseKeyboard(false) }) }}
                    onStopQRScanner={(e) => { this?.props?.onStopQRScanner(e) }}
                    isShowQRScanner={this?.props?.isShowQRScanner}
                />
            </>
        );
    }
}

export default withAlert()(BottomTabBar);
