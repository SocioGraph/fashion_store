import { useState } from 'react';
import Images from '../../assets/images';
import './DropdownSelect.css';
import { InfoBox } from './InfoBoxClassBased';

export default function DropdownSelect(props) {
    const [isListOpen, setIsListOpen] = useState(false)
    const [showInfo1, setShowInfo1] = useState(false);

    const {
        IconUpArrow,
        IconDownArrow,
    } = Images;

    const toggleList = () => {
        setShowInfo1(true)
        setIsListOpen(!isListOpen);
    };

    const selectItem = (item, name) => {
        const { title } = item;
        if (props.customSelector) {
            props.onChangeSelectorValue({ value: title, name })
        } else {
            props.onChangeSelectorValue(title)
        }
        setIsListOpen(false)
    };

    const onClickOutside = () => {
        setShowInfo1(false)
        setIsListOpen(false);
    }

    const manageSelectorBackground = (value) => {
        if (value === 'Perfect fit') {
            return 'transparent linear-gradient(90deg,transparent, #21FF21 100%) 0% 0% no-repeat padding-box';
        }
        if (value === 'Fit') {
            return 'transparent linear-gradient(90deg,transparent, yellow 100%) 0% 0% no-repeat padding-box';
        }
        if (value === 'Long') {
            return 'transparent linear-gradient(90deg,transparent, #1d28a0 100%) 0% 0% no-repeat padding-box';
        }
        if (value === 'Loose') {
            return 'transparent linear-gradient(90deg, #F7FDFF 0%, #FE8989 100%) 0% 0% no-repeat padding-box';
        }
        if (value === 'Tight') {
            return 'transparent linear-gradient(90deg, #F7FDFF 0%, orange 100%) 0% 0% no-repeat padding-box';
        }
    }

    return (
        <>
            <InfoBox
                show={showInfo1}
                onClickOutside={() => {
                    onClickOutside()
                }}
            />
            <div style={{ background: props.customSelector && manageSelectorBackground(props.selectorValue) }} className={`dropdown-select ${props.customSelector ? 'custom-dropdown-select' : 'dropdown-select'} `}>
                <div className="dropdown-select-wrapper">
                    <button disabled={props.isDisabled} type="button" className="dropdown-select-header dropdown-select-btn"
                        onClick={() => toggleList()}>
                        <div
                            className={`dropdown-select-header-title ${props.customSelector ? 'custom-dropdown-select-header-title' : 'dropdown-select-header-title'}`}>{props.selectorValue ? props.selectorValue : props.customTitle ? props.customTitle : props.selectorTitle}</div>

                        {props.customSelector ? (
                            <div className='caret-icon'>
                                <img
                                    className="dropdown-arrow up-down-selector-arrow"
                                    src={IconUpArrow}
                                    aria-hidden="true"
                                />
                                <img
                                    className="dropdown-arrow up-down-selector-arrow"
                                    src={IconDownArrow}
                                    aria-hidden="true"
                                />
                            </div>
                        ) : (
                            isListOpen ? (
                                <img
                                    className="dropdown-arrow up-down-selector-arrow"
                                    src={IconUpArrow}
                                    aria-hidden="true"
                                />
                            ) : (
                                <img
                                    className="dropdown-arrow up-down-selector-arrow"
                                    src={IconDownArrow}
                                    aria-hidden="true"
                                />
                            )

                        )}

                    </button>
                    {isListOpen && (
                        <div className={` ${props.customSelector ? 'custom-dropdown-select-list' : 'dropdown-select-list'}`}>
                            {props?.selectorData?.map((item) => (
                                <button
                                    disabled={props.isDisabled}
                                    type="button"
                                    className="dropdown-select-list-item dropdown-select-btn dropdown-select-title"
                                    style={{
                                        backgroundColor: item.title === props.selectorValue ? '#091398' : '#f1f1f1',
                                        color: item.title === props.selectorValue ? '#ffffff' : '#091398',
                                    }}
                                    key={item.id}
                                    onClick={() => selectItem(item, props.name)}
                                >
                                    {item.title}
                                </button>
                            ))}
                        </div>
                    )}
                </div>
            </div>
        </>
    );
}
