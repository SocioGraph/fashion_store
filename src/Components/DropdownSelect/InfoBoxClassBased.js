import { useEffect, useRef } from 'react';

export function InfoBox(props) {
    let ref = useRef(null);

    const handleClickOutside = (event) => {
        let data = event.path[0].outerHTML.substring(29, 96)
        if (ref.current && !ref.current.contains(event.target) && data !== "dropdown-select-list-item dropdown-select-btn dropdown-select-title") {
            props.onClickOutside && props.onClickOutside();
        }
    };

    useEffect(() => {
        document.addEventListener('click', handleClickOutside, true);
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        };
    });

    if (!props.show)
        return null;

    return <div ref={ref} className='info-box' />
}
