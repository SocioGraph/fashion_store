import React from 'react';
import Modal from 'react-modal';
import IconSend from '../../assets/images'

function ShareQRCodeModal(props) {
  const customStyles = {
    overlay: {
      backgroundColor: '#707070',
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 215,
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      height: "50%",
      width: "70%"
    },
  };
  return (
    <Modal
      isOpen={props.isBarcodeModel}
      onAfterOpen={props.afterOpenModal}
      onRequestClose={props.closeModal}
      contentLabel="Example Modal"
      style={customStyles}
    >
      <div className="flex-direction-column display-flex">
        <div className="justify-content-end display-flex">
          <button
            className="qr-code-close-modal"
            type="button" aria-label="Close" onClick={props.closeModal}>
            <span aria-hidden="true" className="color-red">&times;</span>
          </button>
        </div>
        <div className="qr-code-scan-title">Scan the QR code to get the product
          information on the phone.
        </div>
        <div className="qr-code-box">
          <img src={props.productsBarcode} className="product-barcode-image" />
          {/* <QRCode value="http://facebook.github.io/react/" /> */}
        </div>
        <div className="border-bottom-class margin-top-15" />
        <div className="qr-code-enter-mobile-no-title">Enter a Phone number to receive it
          as SMS
        </div>
        <div className="display-row justify-content-center">
          <div className="input-border-mobile-number">
            <div className="display-row">
              <input
                className="mobile-number-input border-none"
                type="number"
                value={props.mobileNumber}
                onChange={(e) => props.setMobileNumber(e.target.value)} />
              <img
                className="set-icon phone-send-icon"
                src={IconSend}
                aria-hidden="true"
                onClick={() => {
                  props.onSendMobile()
                }}
              />
            </div>
          </div>
        </div>
        <div className="phone-number-error">{props.errorsPhoneNumber}</div>
      </div>
    </Modal>
  );
}

export default ShareQRCodeModal;