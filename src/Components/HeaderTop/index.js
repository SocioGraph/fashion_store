import React from 'react';
import { useAlert } from 'react-alert';
import Images from '../../assets/images';
import history from '../../history';
import Loading from '../../Components/Loading';
import './HeaderTop.css';

const {
    CloseIcon,
    AppLogoHeader,
    IconHome,
    LikeIcon,
    IconBack,
} = Images

function HeaderTop(props) {

    const alert = useAlert();

    const logout = () => {
        history.push('/')
    }

    const onFavoritesPress = () => {
        const favorite_product = JSON.parse(window.localStorage.getItem('favorite_product'));

        if (props.productsInformation) {
            props.onProductsInformation(false)
        } else {
            if (props.isFavoritesProduct) {
                props.onBackPress()
            } else {
                props.onProductFavoritesPress()
            }
        }
        // props.onBackPress()
        // if (favorite_product && favorite_product.length !== 0) {
        //     props.onProductFavoritesPress()
        // } else {
        //     alert.show("Favorite product is not available!");
        // }
    }

    // const onBackNavigation = () => {
    //     if (props.renderScreen === 'Home') {
    //         props.onBack()
    //     } else if (props.renderScreen === 'Product') {
    //         props.onBackPress()
    //     } else {
    //         history.goBack()
    //     }
    // }

    return (
        <>
            <div style={{ display: 'none' }} id="chat_spinner">
                <div className="loading-home-screen-view">
                    <Loading />
                </div>
            </div>
            <div className="row header-top">
                <div className="row home-btn-view">
                    {/* <img
                    src={IconBack}
                    className="header-back-icon"
                    onClick={() => {
                        onBackNavigation()
                    }}
                /> */}
                    <button onClick={() => {
                        logout()
                    }} className="home-btn">
                        <img
                            src={IconHome}
                            className="heart-icon-header"
                        /> Home
                    </button>
                </div>
                <div className="display-flex align-items-center">
                    <img src={AppLogoHeader} className="dave-header-logo" />
                </div>
                <div className="fav-btn-view">
                    {props.renderScreen === 'Home' ?
                        <div className="close-btn" onClick={() => {
                            props.onBack()
                        }}><img src={CloseIcon} alt="close-image" className="scan-qr-close-modal-icon" /></div>
                        :
                        <button className={`btn ${(props.isFavoritesProduct || props.productsInformation) ? 'header-close-btn' : 'fav-btn'}`} onClick={() => {
                            onFavoritesPress()
                        }}>
                            <img
                                src={(props.isFavoritesProduct || props.productsInformation) ? CloseIcon : LikeIcon}
                                className="heart-icon-header"
                            />
                            {(!props.isFavoritesProduct && !props.productsInformation) ? 'Favorites' : ''}
                        </button>
                    }
                </div>
            </div>
        </>
    )
}

export default HeaderTop
