import React, { useRef, useState } from "react";
import Keyboard from "react-simple-keyboard";
import "react-simple-keyboard/build/css/index.css";
import { BottomSheet } from 'react-spring-bottom-sheet';
import 'react-spring-bottom-sheet/dist/style.css';
import './styles.css';

function MyKeyboard(props) {
  const [layout, setLayout] = useState("default");
  const keyboard = useRef();

  const handleShift = () => {
    const newLayoutName = layout === "default" ? "shift" : "default";
    setLayout(newLayoutName);
  };

  const onKeyPress = button => {

    /**
     * If you want to handle the shift and caps lock buttons
     */
    if (button === "{shift}" || button === "{lock}") handleShift();
  };


  const onSubmitValue = () => {
    props.onCloseKeyboard(false)
  }

  return (
    <BottomSheet
      open={props.isKeyboardVisible}
      onSpringStart={async (event) => {
        if (props.Screen === 'QRCode') {
          props.onStopQRScanner(!props.isShowQRScanner)
        }
      }}
    >
      <div
        className="display-flex justify-content-space-between main-keyboard-view"
      >
        <input type="button" value="Cancel"
          className="keyboard-cancel-btn"
          onClick={() => {
            props.onClearValue()
          }}
        />
        <input
          className="input-keyboard"
          placeholder={"Tap on the virtual keyboard to start"}
          value={props.value}
          onChange={(e) => props.onChange(e.target.value)}
        />
        <input type="button" value="Submit"
          className="keyboard-submit-btn"
          onClick={() => {
            onSubmitValue()
          }}
        />
      </div>

      <Keyboard
        keyboardRef={r => (keyboard.current = r)}
        layoutName={layout}
        onChange={(e) => props.onChange(e)}
        onKeyPress={onKeyPress}
      />
    </BottomSheet>
  );
}


export default MyKeyboard

