import React, { useState } from 'react';
import { useAlert } from 'react-alert';
import Modal from 'react-modal';
import BarcodeScannerComponent from 'react-webcam-barcode-scanner';
import { apiActions } from '../../server';
import MyKeyboard from '../MyKeyboard';
import history from '../../history';

function ScanQRCodeModal(props) {
  const [isKeyboardVisible, setIsKeyboardVisible] = useState(false);
  const [isKeyboardLastEvent, setIsKeyboardLastEvent] = useState('');

  const customStylesScanner = {
    overlay: {
      backgroundColor: '#707070',
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 215,
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      height: "100%",
      width: "100%",
      padding: 0,
      backgroundColor: '#707070',
      border: "0px ridge #707070 !important",
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    },
  }

  const alert = useAlert();

  const dave_user_id = window.localStorage.getItem('dave_user_id');
  const dave_engagement_id = window.localStorage.getItem('dave_engagement_id');

  const onSubmitQRCode = () => {
    if (props.isBarcodeScanner) {
      props.setIsLoadingValue(false)
      apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST",
        {
          "engagement_id": dave_engagement_id,
          "refresh_cache": true,
          "system_response": "sr_init",
          "customer_response": props.isBarcodeScanner,
        },
        (res) => {
          document.getElementById("my_transcription").value = '';
          if (res && res.data && res.data.thumbnails && res.data.thumbnails.length !== 0) {
            const currentProduct = res?.data?.thumbnails[0]
            window.localStorage.setItem("gender", currentProduct?.genders[0]);
            window.localStorage.setItem("current_product", JSON.stringify(currentProduct));
            props.setIsLoadingValue(false)
            if (currentProduct?.category_name === 'Top Wear' || currentProduct?.category_name === 'Bottom Wear') {
              props.onFindYourSelectedSize() && props.closeBarcodeScannerModel()
            } else {
              props.closeBarcodeScannerModel()
              history.push({
                pathname: '/product',
              });
            }
          }
          else {
            alert.error("Product not found!");
            props.setIsLoadingValue(false)
          }
        }, (err) => {
          alert.error(err?.responseJSON?.error);
          props.setIsLoadingValue(false)
        }
      )
      // props.getProductFromQRCode(props.isBarcodeScanner)
    } else {
      alert.error("Please align the Barcode within the scanner");
    }
    // if (props.isBarcodeScanner) {
    //   props.onFindYourSelectedSize() && props.closeBarcodeScannerModel()
    // } else {
    //   alert.error("Please align the Barcode within the scanner");
    //   props.setIsBarcodeScanner('')
    // }
  }

  const onChangeInput = (e) => {
    props.setIsBarcodeScanner(e)
  }

  const onCloseKeyboard = (value) => {
    setIsKeyboardVisible(false)
    setIsKeyboardLastEvent(value)
    if (props.isBarcodeScanner) {
      onSubmitQRCode()
    }
    const timer = setTimeout(() => {
      setIsKeyboardLastEvent('')
    }, 1500);
    return () => clearTimeout(timer);
  }

  return (
    <>
      <Modal
        isOpen={props.isBarcodeScannerModel}
        onRequestClose={props.closeBarcodeScannerModel}
        contentLabel="Example Modal"
        style={customStylesScanner}
      >
        <div className="flex-direction-column display-flex align-items-center">
          <div className="qr-scanner-view">
            <div className="scan-any-qr-code-text">Scan Any Barcode</div>
            <div className="qr-image-wrapper-border">
              {/* <div>
              <Scanner />
            </div> */}
              {props.isShowQRScanner &&
                <BarcodeScannerComponent
                  className="qr-image-wrapper"
                  width={350}
                  height={400}
                  stopStream={props.stopStream}
                  onUpdate={(err, result) => {
                    if (result) {
                      props.setIsBarcodeScanner(result.text)
                      props.setStopStream(true)
                    }
                  }}
                />
              }
              {/* <QrReader
              className="qr-image-wrapper"
              delay={300}
              onError={handleError}
              onScan={handleScan}
            /> */}
            </div>
            <label
              htmlFor="formBasicUploadYourPhoto"
              className='choose-qr-code-file-input'>
              <input
                name="uploadUserPhoto"
                type="file"
                accept="image/*"
                id="formBasicUploadYourPhoto"
                className="display-none"
                onChange={(e) => {
                  props.handleFileSelect(e)
                }} />
              <div className='choose-qr-code-file-btn'>
                Choose File
              </div>
            </label>

            {/* {props.isBarcodeScanner &&
            <p className="scan-any-qr-code-data">{props.isBarcodeScanner}</p>
          } */}

            <div className="qr-within-the-scanner"> Please align the Barcode within the Scanner</div>
            <div className="product-code-input-view">
              <input className="product-code-input border-none"
                placeholder="Type here your product code..."
                id="scan_product_code"
                value={props.isBarcodeScanner}
                type="text"
                onFocus={() => {
                  if (isKeyboardLastEvent === '') {
                    setIsKeyboardVisible(true)
                  } else {
                    document.getElementById('scan_product_code').blur()
                  }
                }}
                onChange={(e) => {
                  props.setIsBarcodeScanner(e.target.value)
                }}
              />
            </div>
            <div className='submit-qr-code-result-btn'
              onClick={() => onSubmitQRCode()}
            >
              Submit
            </div>
          </div>
        </div>
      </Modal>
      <MyKeyboard
        onCloseKeyboard={onCloseKeyboard}
        isKeyboardVisible={isKeyboardVisible}
        value={props.isBarcodeScanner}
        onChange={(e) => onChangeInput(e)}
        onClearValue={() => {
          props.setIsBarcodeScanner('')
          onCloseKeyboard(false)
        }}
        Screen="QRCode"
        onStopQRScanner={(e) => { props.onStopQRScanner(e) }}
        isShowQRScanner={props.isShowQRScanner}
      />
    </>
  );
}

export default ScanQRCodeModal;