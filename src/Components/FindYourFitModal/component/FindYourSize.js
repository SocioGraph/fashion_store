import React, { useEffect, useState } from 'react';
import { useAlert } from 'react-alert';
import Images from '../../../assets/images';
import { genderSelector, getFeMaleMandatoryMeasurements, getMaleMandatoryMeasurements } from '../../../data';
import { apiActions } from '../../../server';
import DropdownSelect from '../../DropdownSelect';
import MyKeyboard from '../../MyKeyboard';
import '../FindYourFitModal.css';

const {
    IconBack,
    IconClose,
} = Images

function FindYourFitModal(props) {

    const [yourFitData, setYourFitData] = useState({});
    const [selectGender, setSelectGender] = useState(null);
    const [mandatoryMeasurements, setMandatoryMeasurements] = useState([]);

    const [isNextDisable, setIsNextDisable] = useState(true);
    const [isKeyboardVisible, setIsKeyboardVisible] = useState(false);
    const [isKeyboardLastEvent, setIsKeyboardLastEvent] = useState('');
    const [selectedInputType, setSelectedInputType] = useState('');

    const alert = useAlert();
    const gender = window.localStorage.getItem('gender');

    useEffect(() => {
        let isDisabledButton = null
        for (let i = 0; i < mandatoryMeasurements.length; i++) {
            yourFitData[mandatoryMeasurements[i].name] = Number(mandatoryMeasurements[i].value)
            setYourFitData({ ...yourFitData })
            if (mandatoryMeasurements[i].required) {
                if (mandatoryMeasurements[i].value <= 0) {
                    isDisabledButton = true
                } else if (!isDisabledButton) {
                    isDisabledButton = false
                }
            }
            if (i === mandatoryMeasurements.length - 1) {
                setIsNextDisable(isDisabledButton)
            }
        }
    }, [mandatoryMeasurements]);

    useEffect(() => {
        const currentProduct = JSON.parse(window.localStorage.getItem('current_product'));
        if (currentProduct) {
            let getMeasurements = currentProduct?.genders[0] === 'male' ? getMaleMandatoryMeasurements[currentProduct.category_name] : getFeMaleMandatoryMeasurements[currentProduct.category_name]
            if (!getMeasurements) {
                setMandatoryMeasurements(currentProduct?.genders[0] === 'male' ? getMaleMandatoryMeasurements['Normal'] : getFeMaleMandatoryMeasurements['Normal'])
            } else {
                setMandatoryMeasurements(getMeasurements)
            }
        }
    }, [])

    const onChangeInput = (index, value) => {
        if (value) {
            mandatoryMeasurements[index].value = value

        } else {
            mandatoryMeasurements[index].value = 0
        }
        setMandatoryMeasurements([...mandatoryMeasurements])
    }

    const onChangeStep = (value) => {
        if (!isNextDisable) {
            props.onChangeStep(value)
        }
    }
    const dave_engagement_id = window.localStorage.getItem('dave_engagement_id');
    const dave_user_id = window.localStorage.getItem('dave_user_id');

    const onClickNext = () => {
        props.setIsLoadingValue(true)
        const body = {
            ...yourFitData,
            gender: gender,
            is_inches: !props.isSizeSwitch,
        }
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
            "engagement_id": dave_engagement_id,
            "refresh_cache": true,
            "system_response": "sr_init",
            "customer_response": JSON.stringify(body),
            "customer_state": "cs_submit_size_recommendation",

        },
            (res) => {
                if (res?.data?.size) {
                    window.localStorage.setItem("size_recommendation", JSON.stringify(res));
                    onChangeStep(2)
                }
                if (res?.data?.error) {
                    alert.error(res?.data?.error);
                }
                props.setIsLoadingValue(false)
            },
            (err) => {
                props.setIsLoadingValue(false)
                alert.error(err?.responseJSON?.error);
            }
        )
    }

    const handleChangeStyle = (value) => {
        setSelectGender(value)
        window.localStorage.setItem("gender", value);

        props.setIsLoadingValue(true)
        const data = {
            colour: '',
            brand: '',
            sub_category: value === 'male' ? 'casual shirts' : 'dresses',
            order: true,
            gender: value,
            size: '',

        }
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST",
            {
                "engagement_id": dave_engagement_id,
                "refresh_cache": true,
                "system_response": "sr_init",
                "customer_response": JSON.stringify(data),
                "customer_state": "cs_category"
            },
            (response) => {
                if (response?.data?.thumbnails) {
                    if (response && response.data && response.data.thumbnails && response.data.thumbnails.length !== 0) {
                        const currentProduct = response?.data?.thumbnails[0]
                        window.localStorage.setItem("current_product", JSON.stringify(currentProduct));
                        props.setIsLoadingValue(false)
                        let getMeasurements = value === 'male' ? getMaleMandatoryMeasurements[currentProduct.category_name] : getFeMaleMandatoryMeasurements[currentProduct.category_name]
                        if (!getMeasurements) {
                            setMandatoryMeasurements(value === 'male' ? getMaleMandatoryMeasurements['Normal'] : getFeMaleMandatoryMeasurements['Normal'])
                        } else {
                            setMandatoryMeasurements(getMeasurements)
                        }
                    }
                }
                if (response?.data?.error) {
                    alert.error(response?.data?.error);
                    props.setIsLoadingValue(false)
                }
            }, (error) => {
                alert.error(error.responseJSON.error);
                props.setIsLoadingValue(false)
            });
    }

    const onCloseKeyboard = (value) => {
        setIsKeyboardVisible(false)
        setSelectedInputType('')
        setIsKeyboardLastEvent(value)
        const timer = setTimeout(() => {
            setIsKeyboardLastEvent('')
        }, 1500);
        return () => clearTimeout(timer);
    }

    return (
        <div className="find-your-size-modal">
            <div className="find-your-size-modal-header">
                <div>
                    {props.isGoBack &&
                        <img
                            src={IconBack}
                            className="find-your-size-modal-back-icon"
                            onClick={() => {
                                props.onFindYourSize()
                            }}
                        />
                    }
                    <h1 className="find-your-size-modal-header-title">Find Your Fit.</h1>
                </div>
                <div onClick={() => { props.onFindYourSize() }}>
                    <img
                        src={IconClose}
                        className="cursor-pointer find-your-size-modal-back-icon"
                    />
                </div>
            </div>
            {props.isShowGenderSelection &&
                <div className="find-your-size-modal-body" style={{ marginTop: 20 }}>
                    <div className="measurement-type-view">
                        <h2 className="enter-your-size-title">Select Your Gender</h2>
                    </div>
                    <div style={{ width: 300, border: '2px solid #0a1498', borderRadius: 10, marginTop: 10 }}>
                        <DropdownSelect
                            selectorTitle="Select your gender"
                            selectorData={genderSelector}
                            selectorValue={selectGender}
                            onChangeSelectorValue={(e) => handleChangeStyle(e)}
                        />
                    </div>
                </div>
            }
            {((props.isShowGenderSelection && selectGender) || !props.isShowGenderSelection) &&
                <>
                    <div className="find-your-size-modal-body">
                        <div className="measurement-type-view">
                            <h2 className="enter-your-size-title">Enter Your Size</h2>
                            <div className="measurement-type-switch-main">
                                IN
                                <div className="measurement-type-switch" style={{
                                    justifyContent: props.isSizeSwitch ? 'flex-end' : 'flex-start',
                                    padding: props.isSizeSwitch ? '0 5px 0 0' : '0 0 0 5px',
                                    backgroundColor: props.isSizeSwitch ? '#0a1498' : '#545454',
                                }}
                                    onClick={() => {
                                        props.onSetIsSizeSwitch(!props.isSizeSwitch)
                                    }}
                                >
                                    <div className="measurement-type-switch-on-off" />
                                </div>
                                CM
                            </div>
                        </div>
                    </div>
                    <div className="measurement_size_main_form_group">
                        {mandatoryMeasurements && mandatoryMeasurements.map((item, index) => {
                            return (
                                <div key={index} className="measurement_size_form_group field">
                                    <input
                                        type="number"
                                        className="measurement_size_form_field"
                                        placeholder={item.title}
                                        name={item.name}
                                        id={`${item.name}_measurement`}
                                        required={item.required}
                                        value={item.value}
                                        onChange={(e) => {
                                            onChangeInput(index, e.target.value)
                                        }}
                                        onFocus={() => {
                                            if (isKeyboardLastEvent === '') {
                                                setIsKeyboardVisible(true)
                                                setSelectedInputType(index)
                                            } else {
                                                document.getElementById(`${item.name}_measurement`).blur()
                                            }
                                        }}
                                    />
                                    {item.value !== '' &&
                                        <p className="measurement_size_type">{props.isSizeSwitch ? '(cm)' : '(in)'}</p>
                                    }
                                    <label htmlFor={item.name} className="measurement_size_form_label">{item.title} {item.required && '*'}</label>
                                </div>
                            )
                        })}


                        {/* <div className="measurement_size_form_group field">
                            <input
                                type="number"
                                className="measurement_size_form_field"
                                placeholder="Height"
                                name="height"
                                id="height_measurement"
                                required=""
                                value={yourFitData.height}
                                onChange={(e) => onChangeInput('height', e.target.value)}
                                onFocus={() => {
                                    if (isKeyboardLastEvent === '') {
                                        setIsKeyboardVisible(true)
                                        setSelectedInputType('height')
                                    } else {
                                        document.getElementById('height_measurement').blur()
                                    }
                                }}
                            />
                            {yourFitData.height !== '' &&
                                <p className="measurement_size_type">{props.isSizeSwitch ? '(cm)' : '(in)'}</p>
                            }
                            <label htmlFor="height" className="measurement_size_form_label">Height</label>
                        </div>
                        <div className="measurement_size_form_group field">
                            <input
                                type="number"
                                className="measurement_size_form_field"
                                placeholder="Collar"
                                name="collar"
                                id="collar_measurement"
                                required=""
                                value={yourFitData.collar}
                                onChange={(e) => onChangeInput('collar', e.target.value)}
                                onFocus={() => {
                                    if (isKeyboardLastEvent === '') {
                                        setIsKeyboardVisible(true)
                                        setSelectedInputType('collar')
                                    } else {
                                        document.getElementById('collar_measurement').blur()
                                    }
                                }}
                            />
                            {yourFitData.collar !== '' &&
                                <p className="measurement_size_type">{props.isSizeSwitch ? '(cm)' : '(in)'}</p>
                            }
                            <label htmlFor="collar" className="measurement_size_form_label">Collar</label>
                        </div>
                        <div className="measurement_size_form_group field">
                            <input
                                type="number"
                                className="measurement_size_form_field"
                                placeholder="Shoulder"
                                name="shoulder"
                                id="shoulder_measurement"
                                required=""
                                value={yourFitData.shoulder}
                                onChange={(e) => onChangeInput('shoulder', e.target.value)}
                                onFocus={() => {
                                    if (isKeyboardLastEvent === '') {
                                        setIsKeyboardVisible(true)
                                        setSelectedInputType('shoulder')
                                    } else {
                                        document.getElementById('shoulder_measurement').blur()
                                    }
                                }}
                            />
                            {yourFitData.shoulder !== '' &&
                                <p className="measurement_size_type">{props.isSizeSwitch ? '(cm)' : '(in)'}</p>
                            }
                            <label htmlFor="shoulder" className="measurement_size_form_label">Shoulder</label>
                        </div>
                        <div className="measurement_size_form_group field">
                            <input
                                type="number"
                                className="measurement_size_form_field"
                                placeholder="Waist"
                                name="waist"
                                id="waist_measurement"
                                required=""
                                value={yourFitData.waist}
                                onChange={(e) => onChangeInput('waist', e.target.value)}
                                onFocus={() => {
                                    if (isKeyboardLastEvent === '') {
                                        setIsKeyboardVisible(true)
                                        setSelectedInputType('waist')
                                    } else {
                                        document.getElementById('waist_measurement').blur()
                                    }
                                }}
                            />
                            {yourFitData.waist !== '' &&
                                <p className="measurement_size_type">{props.isSizeSwitch ? '(cm)' : '(in)'}</p>
                            }
                            <label htmlFor="waist" className="measurement_size_form_label">Waist</label>
                        </div>
                        <div className="measurement_size_form_group field">
                            <input
                                type="number"
                                className="measurement_size_form_field"
                                placeholder="Chest"
                                name="chest"
                                id="chest_measurement"
                                required=""
                                value={yourFitData.chest}
                                onChange={(e) => onChangeInput('chest', e.target.value)}
                                onFocus={() => {
                                    if (isKeyboardLastEvent === '') {
                                        setIsKeyboardVisible(true)
                                        setSelectedInputType('chest')
                                    } else {
                                        document.getElementById('chest_measurement').blur()
                                    }
                                }}
                            />
                            {yourFitData.chest !== '' &&
                                <p className="measurement_size_type">{props.isSizeSwitch ? '(cm)' : '(in)'}</p>
                            }
                            <label htmlFor="chest" className="measurement_size_form_label">Chest</label>
                        </div>
                        <div className="measurement_size_form_group field">
                            <input
                                type="number"
                                className="measurement_size_form_field"
                                placeholder="Hip"
                                name="hip"
                                id="hip_measurement"
                                required=""
                                value={yourFitData.hip}
                                onChange={(e) => onChangeInput('hip', e.target.value)}
                                onFocus={() => {
                                    if (isKeyboardLastEvent === '') {
                                        setIsKeyboardVisible(true)
                                        setSelectedInputType('hip')
                                    } else {
                                        document.getElementById('hip_measurement').blur()
                                    }
                                }}
                            />
                            {yourFitData.hip !== '' &&
                                <p className="measurement_size_type">{props.isSizeSwitch ? '(cm)' : '(in)'}</p>
                            }
                            <label htmlFor="hip" className="measurement_size_form_label">Hip</label>
                        </div> */}
                    </div>
                </>
            }

            <div className="next-step-btn">
                <div className={`next-step-btn-title ${isNextDisable && "disabled-next-step-btn"}`} onClick={() => {
                    !isNextDisable && onClickNext()
                }}>
                    NEXT
                </div>
            </div>
            <MyKeyboard
                Screen="Normal"
                onCloseKeyboard={(e) => onCloseKeyboard(e)}
                isKeyboardVisible={isKeyboardVisible}
                value={mandatoryMeasurements[selectedInputType]?.value}
                onChange={(e) => onChangeInput(selectedInputType, e)}
                onClearValue={() => {
                    onChangeInput(selectedInputType, 0)
                    setSelectedInputType('')
                    onCloseKeyboard(false)
                }}
            />
        </div >
    )
}

export default FindYourFitModal;
