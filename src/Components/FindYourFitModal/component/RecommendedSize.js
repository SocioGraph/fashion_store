import React, { useEffect, useState } from 'react';
import Images from '../../../assets/images';
import { fitnessSelector } from '../../../data';
import DropdownSelect from '../../DropdownSelect';
import Loading from '../../Loading';
import '../FindYourFitModal.css';

const {
    BlueIconArrowRight,
    BlueIconArrowLeft,
    IconBack,
    IconClose
} = Images

function RecommendedSize(props) {
    const [currentProduct, setCurrentProduct] = useState(null);
    const [sizeRecommendation, setSizeRecommendation] = useState(null);
    const [size, setSize] = useState(0)
    const [isLoading, setIsLoading] = useState(true)
    const [sizeList, setSizeList] = useState([])
    const [selectFitness, setSelectFitness] = useState({
        height: 0,
        collar: 0,
        shoulder: 0,
        waist: 0,
        chest: 0,
    });

    const handleChange = (e) => {
        const { name, value } = e;
        setSelectFitness({
            ...selectFitness,
            [name]: value,
        })
    };

    const manageSize = (type) => {
        if (type === 'Add' && size < sizeList.length - 1) {
            setSize(size + 1)
        } else if (type === 'Remove' && size > 0) {
            setSize(size - 1)
        }
    }

    useEffect(() => {
        const current_product = JSON.parse(window.localStorage.getItem('current_product'));
        const size_recommendation = JSON.parse(window.localStorage.getItem('size_recommendation'));
        if (current_product) {
            const objSize = Object.keys(current_product).length
            if (objSize !== 0) {
                setCurrentProduct(current_product)
            }
        }
        const recommendationObjSize = Object.keys(size_recommendation.data).length
        if (size_recommendation && recommendationObjSize !== 0) {

            const objSize = Object.keys(size_recommendation).length
            if (objSize !== 0) {
                setSizeRecommendation(size_recommendation)
            }
            const count = Object.keys(size_recommendation.data.size)
            setSizeList(count)
            if (size_recommendation.data.fit_size) {
                for (let i = 0; i < count.length - 1; i++) {
                    if (count[i] == size_recommendation.data.fit_size) {
                        setSize(i)
                    }
                }
            }
        }
    }, [])

    useEffect(() => {
        if (sizeRecommendation) {
            setIsLoading(false)
            if (sizeRecommendation.data.size) {
                setSelectFitness(
                    Object.values(sizeRecommendation?.data?.size)[size],
                )
            }
            props.onChangeMySize(sizeList[size])
        }
    }, [sizeRecommendation, size])


    const onChangeStep = (value) => {
        props.onChangeStep(value)
    }


    return (
        <div className="find-your-size-modal">
            <div className="find-your-size-modal-header find-your-size-modal-header-step-2">
                <div>
                    {props.isGoBack &&
                        <img
                            src={IconBack}
                            className="header-back-icon"
                            onClick={() => {
                                onChangeStep(1)
                            }}
                        />
                    }
                    <h1 className="find-your-size-modal-header-title">Recommended Size</h1>
                </div>
                <div onClick={() => { props.onFindYourSize() }} >
                    <img
                        src={IconClose}
                        className="cursor-pointer find-your-size-modal-back-icon"
                    />
                </div>
            </div>
            <div className='product-image-and-content'>
                <div>
                    <img src={currentProduct?.image[0]} className='product-image' alt='product-image' />
                </div>
                <div className='product-details'>
                    <div>
                        <p className='brand-name'> {currentProduct?.brand_name} </p>
                        <h1 className='product-name'>{currentProduct?.product_name}</h1>
                        {/*<span className='original-cost strikethrough'>{currentProduct?.price}</span>*/}
                        <h1 className='discounted-cost'>₹ {currentProduct?.selling_price}</h1>
                    </div>

                    <div className='size-selector'>
                        <p className='size-selector-label'>Size</p>
                        <div className='size-selector-button'>
                            <img
                                onClick={() => manageSize('Remove')}
                                src={BlueIconArrowLeft}
                                className="size-selector-arrow-left manage-size-icon"
                                aria-hidden="true"
                            />
                            <p className='size-number'>{sizeList[size]}</p>
                            <img
                                onClick={() => manageSize('Add')}
                                src={BlueIconArrowRight}
                                className="size-selector-arrow-right manage-size-icon"
                                aria-hidden="true"
                            />
                        </div>
                    </div>

                </div>
            </div>

            <div className='cloth-fitting'>
                {isLoading ?
                    <Loading />
                    :
                    <>
                        <div className='selector-view'>
                            <p className="selector-title">Height</p>
                            <DropdownSelect
                                isDisabled={true}
                                name='height'
                                customSelector
                                selectorTitle="Choose heigh"
                                selectorData={fitnessSelector}
                                selectorValue={selectFitness.height}
                                onChangeSelectorValue={handleChange}
                            />
                        </div>
                        <div className='selector-view'>
                            <p className="selector-title">Collar</p>
                            <DropdownSelect
                                isDisabled={true}
                                name='collar'
                                customSelector
                                selectorTitle="Choose collar"
                                selectorData={fitnessSelector}
                                selectorValue={selectFitness.collar}
                                onChangeSelectorValue={handleChange}
                            />
                        </div>
                        <div className='selector-view'>
                            <p className="selector-title">Shoulder</p>
                            <DropdownSelect
                                isDisabled={true}
                                name='shoulder'
                                customSelector
                                selectorTitle="Choose shoulder"
                                selectorData={fitnessSelector}
                                selectorValue={selectFitness.shoulder}
                                onChangeSelectorValue={handleChange}
                            />
                        </div>
                        <div className='selector-view'>
                            <p className="selector-title">Chest</p>
                            <DropdownSelect
                                isDisabled={true}
                                name='chest'
                                customSelector
                                selectorTitle="Choose chest"
                                selectorData={fitnessSelector}
                                selectorValue={selectFitness.chest}
                                onChangeSelectorValue={handleChange}
                                measure
                            />
                        </div>
                        <div className='selector-view'>
                            <p className="selector-title">Waist</p>
                            <DropdownSelect
                                isDisabled={true}
                                name='waist'
                                customSelector
                                selectorTitle="Choose waist"
                                selectorData={fitnessSelector}
                                selectorValue={selectFitness.waist}
                                onChangeSelectorValue={handleChange}
                            />
                        </div>
                    </>
                }
            </div>
            <div className="next-step-btn">
                <div className="next-step-btn-title recommended-new-size-step-btn-title" onClick={() => {
                    props.onChangeStep(3)
                }}>
                    New Size
                </div>
            </div>
        </div >
    )
}

export default RecommendedSize;
