import React from 'react';
import { useAlert } from 'react-alert';
import history from '../../../history';
import { apiActions } from '../../../server';
import '../FindYourFitModal.css';

function PreferredSize(props) {
  const dave_engagement_id = window.localStorage.getItem('dave_engagement_id');
  const dave_user_id = window.localStorage.getItem('dave_user_id');
  const alert = useAlert();
  const gender = window.localStorage.getItem('gender');

  const onChangeStep = (value) => {
    props.setIsLoadingValue(true)
    window.localStorage.setItem("my_size", props.mySize);
    props.onFindYourSize()
    const data = {
      colour: '',
      brand: '',
      sub_category: '',
      order: true,
      gender: gender,
      size: props.mySize,

    }
    apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST",
      {
        "engagement_id": dave_engagement_id,
        "refresh_cache": true,
        "system_response": "sr_init",
        "customer_response": JSON.stringify(data),
        "customer_state": "cs_category"
      },
      (response) => {
        if (response) {
          document.getElementById("my_transcription").value = '';
          if (response && response.data && response.data.thumbnails && response.data.thumbnails.length !== 0) {
            const currentProduct = response?.data?.thumbnails[0]
            window.localStorage.setItem("current_product", JSON.stringify(currentProduct));
            response.data.thumbnails.shift();
            window.localStorage.setItem("similar_products", JSON.stringify(response));
            if (currentProduct && currentProduct.product_code) {
              getRecommendedProducts(currentProduct?.product_code)
            } else {
              const value = {
                data: {
                  thumbnails: []
                }
              }
              window.localStorage.setItem("recommended_products", JSON.stringify(value));
              history.push({
                pathname: '/product',
              });
              props.setIsLoadingValue(true)
            }
          }
          if (response?.data?.error) {
            alert.error(response?.data?.error);
            props.setIsLoadingValue(false)
          }
        }
      },
      (error) => {
        alert.error(error.responseJSON.error);
        props.setIsLoadingValue(true)
      });
  }

  const getRecommendedProducts = (product_code) => {
    apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
      "engagement_id": dave_engagement_id,
      "refresh_cache": true,
      "system_response": "sr_init",
      "customer_response": product_code,
      "customer_state": "cs_active_nudge_recommendations"
    },
      (resp) => {

        if (resp?.data?.thumbnails) {
          window.localStorage.setItem("recommended_products", JSON.stringify(resp));
        } else {
          let value = resp;
          value.data['thumbnails'] = []

          window.localStorage.setItem("recommended_products", JSON.stringify(value));
        }
        if (resp?.data?.error) {
          alert.error(resp?.data?.error);
        }
        props.setIsLoadingValue(false)
        history.push({
          pathname: '/product',
        });
      },
      (err) => {
        alert.error(err?.responseJSON?.error);
        props.setIsLoadingValue(false)
      }
    )
  }


  return (
    <div className="preferred-size-modal">
      <div>
        <p className="preferred-size-modal-title">Your preferred size {props.mySize} for this product is "out of stock."</p>
        <div className="next-step-btn">
          <div className='previous-step-btn-title'
            onClick={() => {
              onChangeStep(props.stepNumber)
            }}>
            Explore Size {props.mySize} products
          </div>
          <div className='go-home-btn-title'
            onClick={() => {
              props.onFindYourSize()
            }}>
            Go Home
          </div>
        </div>
      </div>
    </div>
  )
}

export default PreferredSize;
