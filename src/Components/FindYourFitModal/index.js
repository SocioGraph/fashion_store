import React, { useEffect, useState } from 'react';
import FindYourSize from './component/FindYourSize';
import PreferredSize from './component/PreferredSize';
import RecommendedSize from './component/RecommendedSize';
import './FindYourFitModal.css';

function FindYourFitModal(props) {
    const [currentStep, setCurrentStep] = useState(null)
    const [mySize, setMySize] = useState(0)

    useEffect(() => {
        setCurrentStep(props.stepNumber)
    }, [])

    const onChangeStep = (value) => {
        setCurrentStep(value)
    }

    const onChangeMySize = (value) => {
        setMySize(value)
    }

    return (
        <div className={`${currentStep === 3 ? 'preferred-size-main-modal' : 'find-your-size-modal'}`}>
            {currentStep === 1 && <FindYourSize
                onChangeStep={(e) => onChangeStep(e)}
                onSetIsSizeSwitch={(e) => props.onSetIsSizeSwitch(e)}
                isSizeSwitch={props.isSizeSwitch}
                onFindYourSize={props.onFindYourSize}
                /* isGoBack={props.isGoBack} */
                isGoBack={false}
                setIsLoadingValue={props.setIsLoadingValue}
                isShowGenderSelection={props.isShowGenderSelection}
            />}
            {currentStep === 2 && <RecommendedSize
                onChangeStep={(e) => onChangeStep(e)}
                onSetIsSizeSwitch={(e) => props.onSetIsSizeSwitch(e)}
                isSizeSwitch={props.isSizeSwitch}
                onFindYourSize={props.onFindYourSize}
                isGoBack={props.isGoBack}
                setIsLoadingValue={props.setIsLoadingValue}
                onChangeMySize={onChangeMySize}
            />}
            {currentStep === 3 && <PreferredSize
                onChangeStep={(e) => onChangeStep(e)}
                isSizeSwitch={props.isSizeSwitch}
                onFindYourSize={props.onFindYourSize}
                isGoBack={props.isGoBack}
                setIsLoadingValue={props.setIsLoadingValue}
                mySize={mySize}
                stepNumber={props.stepNumber}
            />}
        </div>
    )
}

export default FindYourFitModal;
