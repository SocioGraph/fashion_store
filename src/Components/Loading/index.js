import React, { useEffect, useState } from 'react';
import './loading.css';

const Loading = () => {
    const [currentIndex, setCurrentIndex] = useState(0);


    useEffect(() => {
        let changeIndex = setTimeout(function sayHi() {
            setCurrentIndex(1)
        }, 500);
        return changeIndex
    }, []);

    useEffect(() => {
        let changeIndex = setTimeout(function sayHi() {
            if (currentIndex === 3) {
                setCurrentIndex(1)
            } else {
                setCurrentIndex(currentIndex + 1)
            }
        }, 500);
        return changeIndex
    }, [currentIndex]);

    return (
        <div className="loading">
            <div className={`${currentIndex === 1 ? 'loading-inactive' : 'loading-active'}`} />
            <div className={`${currentIndex === 2 ? 'loading-inactive' : 'loading-active'}`} />
            <div className={`${currentIndex === 3 ? 'loading-inactive' : 'loading-active'}`} />
        </div>
    );
}

export default Loading;
