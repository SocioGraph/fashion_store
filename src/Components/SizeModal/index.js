import React from 'react';
import Modal from 'react-modal';

function SizeModal(props) {
  const customBlurSelectedModalStyles = {
    overlay: {
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: 'transparent',
      backdropFilter: 'blur(44px)',
      zIndex: 999
    },
    content: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      border: '1px solid #ccc',
      background: 'transparent',
      overflow: 'auto',
      WebkitOverflowScrolling: 'touch',
      borderRadius: '4px',
      outline: 'none',
      padding: '20px',
      height: "100%",
      width: '100%',
    }
  };

  const current_product = JSON.parse(window.localStorage.getItem('current_product'));

  console.log('current_product====', current_product)
  return (
    <Modal
      isOpen={props.isSelectOpenSizeModal}
      onRequestClose={props.onFindYourSelectedSize}
      contentLabel="Example Modal"
      style={customBlurSelectedModalStyles}
    >
      <div
        className="height-100 display-flex justify-content-center align-items-center">
        <div className="did-the-product-fit-modal">
          <img
            src={current_product?.image[0]}
            className="selected-product-image"
          />
          <div className="selected-product-image-view">
            <p className='did-the-product-fit-modal-title'>Did the {current_product?.category_name} fit you well?</p>
            <div className="yes-no-modal-footer-view">
              <div className="yes-no-modal-footer-btn" onClick={() => {
                props.onYesPress()
              }}>
                <div className="information-title">Yes</div>
              </div>
              <div className="yes-no-modal-footer-btn" onClick={() => {
                props.onFindYourSize()
              }}>
                <div className="information-title">No</div>
              </div>
            </div>
          </div>
        </div >
      </div>
    </Modal >
  );
}

export default SizeModal;