import React from 'react';
import { positions, Provider, transitions } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import './App.css';
import Routes from './Routes';

class App extends React.Component {
    render() {
        const options = {
            timeout: 3000,
            position: positions.TOP_RIGHT,
            transition: transitions.FADE
        };
        return (
            <Provider template={AlertTemplate} {...options}>
                <React.Fragment>
                    <Routes />
                </React.Fragment>
            </Provider>
        );
    }
}

export default App;
