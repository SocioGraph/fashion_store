import $ from "jquery";

// export default { signup };
export const apiActions = {
    signup,
    get_product,
    search_products,
    get_options,
    patch_user,
    login,
    do_interaction,
    get_similar_products,
    get_recommended_look,
    create_session,
    logout,
    get_adverts,
    remove_interaction,
    get_interactions,
};


var DAVE_SETTINGS = {
    BASE_URL: "https://test.iamdave.ai",
    SIGNUP_API_KEY: "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__",
    ENTERPRISE_ID: "dave_fashion",
    USER_MODEL: "customer",
    USER_ID: "customer_id",
    SESSION_MODEL: "session",
    SESSION_ID: "session_id",
    SESSION_USER_ID: "customer_id",
    SESSION_ORIGIN: "interaction_origin",
    INTERACTION_MODEL: "interaction",
    INTERACTION_STAGE_ATTR: "stage",
    INTERACTION_USER_ID: "customer_id",
    INTERACTION_ORIGIN: "interaction_origin",
    PRODUCT_MODEL: "product",
    CATEGORY_MODEL: "product_category",
    CONVERSATION_ID: "deployment_kiosk",
    PRODUCT_ID: "product_id",
    PRODUCT: {},
    DEFAULT_USER_DATA: {
        store_id: 'store_id',
        device_id: 'device_id',
        validated: true,
    },
};

function signup(data, callbackFunc, errorFunc) {
    var signupurl =
        DAVE_SETTINGS.BASE_URL + "/customer-signup/" + DAVE_SETTINGS.USER_MODEL;
    //Password string generator
    var randomstring = Math.random()
        .toString(36)
        .slice(-8);
    data = data || {};
    if (!data.customer_name) {
        data.customer_name = randomstring;
    }
    if (!data.email) {
        data.email = "ananth+" + data.customer_name + "@iamdave.ai";
    }
    data["device_id"] = getCookie("device_id");
    data["store_id"] = getCookie("store_id");
    for (let k in DAVE_SETTINGS.DEFAULT_USER_DATA) {
        if (!data[k]) {
            data[k] = DAVE_SETTINGS.DEFAULT_USER_DATA[k];
        }
    }

    $.ajax({
        url: signupurl,
        method: "POST",
        dataType: "json",
        contentType: "json",
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
            "X-I2CE-SIGNUP-API-KEY": DAVE_SETTINGS.SIGNUP_API_KEY,
        },
        data: JSON.stringify(data),
        success: function (data) {
            let HEADERS = {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
                "X-I2CE-USER-ID": data[DAVE_SETTINGS.USER_ID],
                "X-I2CE-API-KEY": data.api_key,
            };
            setCookie("user_authentication", JSON.stringify(HEADERS), 24);
            setCookie("user_id", data[DAVE_SETTINGS.USER_ID]);
            get_interactions();
            if (callbackFunc) {
                callbackFunc(data);
            }
        },
        error: function (r, e) {
            if (errorFunc) {
                errorFunc(r, e);
            }
        },
    });
}

// Login function
// use the cookie store_logo to display the logo at the top
// use the store_name
function login(user_name, password, callbackFunc, errorFunc) {
    let params = {};
    params["user_id"] = user_name;
    params["password"] = password;
    params["roles"] = "device";
    params["attrs"] = ["email", "device_id"];
    params["enterprise_id"] = DAVE_SETTINGS.ENTERPRISE_ID;
    ajaxRequestWithData(
        "/dave/oauth",
        "POST",
        JSON.stringify(params),
        function (data) {
            let HEADERS = {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
                "X-I2CE-USER-ID": data.user_id,
                "X-I2CE-API-KEY": data.api_key,
            };
            setCookie("authentication", JSON.stringify(HEADERS), 24);
            setCookie("device_id", data["user_id"]);
            clearCookie("session_id");
            clearCookie("user_id");
            ajaxRequest('/object/device/' + data['user_id'], 'GET', function (data1) {
                DAVE_SETTINGS.DEFAULT_USER_DATA.store_id = data1.store_id;
                DAVE_SETTINGS.DEFAULT_USER_DATA.device_id = data1.device_id;
                for (let h of ["store_logo", "store_id", "store_name", "city", "country", "state", "pincode", "bar_code_formats"]) {
                    if (data1[h]) {
                        setCookie(h, data1[h]);
                    }
                }
            })
            callbackFunc(data);
        },
        errorFunc,
    );
}

function logout(callbackFunc, errorFunc) {
    clearCookie("user_id");
    clearCookie("user_authentication");
    clearCookie("favourites");
    let session_id = getCookie("session_id");
    signup({}, function () {
        if (session_id) {
            ajaxRequestWithData(
                "/object/" + DAVE_SETTINGS.SESSION_MODEL + "/" + session_id,
                "PATCH",
                JSON.stringify({}),
                function () {
                    clearCookie("session_id");
                    if (callbackFunc) {
                        callbackFunc();
                    }
                }
            );
        }
    }, errorFunc);
}

// You can get the column names of any model with the following API
// ajaxRequestWithData("/attributes/<model_name>/name", "GET",
//
// When the customer signs up, add the params, name, username, password, email, and phone_number attributes, and update the customer
function patch_user(params, callbackFunc, errorFunc) {
    // e.g. patch_user({"pincode": <pincode>, "name": <name of user>, "company_name": <company_name>}, function(data) {});
    // In the response you will get the warehouse_id
    params = params || {}
    let user_id = getCookie("user_id");
    if (params.customer_name && params.email && params.phone_number) {
        params["signed_up"] = true;
        setCookie("logged_in", true);
    }
    ajaxRequestWithData(
        "/object/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id,
        "PATCH",
        JSON.stringify(params),
        function (data) {
            for (let k in ["name", "email", "phone_number", "customer_id"]) {
                if (data[k]) {
                    setCookie("customer_" + k, data[k]);
                }
            }
            if (callbackFunc) {
                callbackFunc(data);
            }
        },
        (e) => {
            errorFunc(e.responseText);
        }
    );
}

// params can include
// products (in case product is selected in the filter)

function do_interaction(
    product_id,
    stage,
    quantity,
    params,
    callbackFunc,
    errorFunc,
    repeats
) {
    if (
        !signup_guard(do_interaction, [
            product_id,
            stage,
            quantity,
            params,
            callbackFunc,
            errorFunc,
            repeats || 0,
        ], 'user_id')
    ) {
        return;
    }
    let user_id = getCookie("user_id");
    stage = stage || "favourite";
    if (!params) {
        params = {};
    }
    params["device_id"] = getCookie("device_id");
    params["store_id"] = getCookie("store_id");
    params["session_id"] = getCookie("session_id");
    params[DAVE_SETTINGS.INTERACTION_ORIGIN] = window.location.href;
    params[DAVE_SETTINGS.USER_ID] = user_id;
    params[DAVE_SETTINGS.PRODUCT_ID] = product_id;
    params[DAVE_SETTINGS.INTERACTION_STAGE_ATTR] = stage;
    params[DAVE_SETTINGS.QUANTITY_ATTR] = (quantity || 1);
    params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;
    params["_async"] = true;
    ajaxRequestWithData(
        "/object/" + DAVE_SETTINGS.INTERACTION_MODEL,
        "POST",
        JSON.stringify(params),
        callbackFunc,
        errorFunc
    );
    get_interactions();
    if (stage == 'favourite') {
        let rd = getCookie('favourites', {});
        let pd = getCookie('products');
        if (pd[product_id]) {
            rd[product_id] = pd[product_id];
        } else {
            rd[product_id] = null;
            get_product(product_id, function (data1) {
                let rd1 = getCookie('favourites', {});
                if (rd1[product_id]) {
                    rd1[product_id] = data1;
                    setCookie('favourites', rd1);
                }
            });
        }
    }
}

function remove_interaction(
    product_id,
    stage,
    params,
    callbackFunc,
    errorFunc,
    repeats
) {
    if (
        !signup_guard(remove_interaction, [
            product_id,
            stage,
            params,
            callbackFunc,
            errorFunc,
            repeats || 0,
        ], 'user_id')
    ) {
        return;
    }
    let user_id = getCookie("user_id");
    stage = stage || "favourite";
    if (!params) {
        params = {};
    }
    params[DAVE_SETTINGS.USER_ID] = user_id;
    params[DAVE_SETTINGS.PRODUCT_ID] = product_id;
    params[DAVE_SETTINGS.INTERACTION_STAGE_ATTR] = stage;
    params["_async"] = true;
    ajaxRequestWithData(
        "/objects/" + DAVE_SETTINGS.INTERACTION_MODEL,
        "DELETE",
        JSON.stringify(params),
        callbackFunc,
        errorFunc
    );
    let rd = getCookie('favourites', {});
    delete rd[product_id];
    setCookie('favourites', rd);
}

function get_interactions(
    stage,
    params,
    callbackFunc,
    errorFunc,
    repeats
) {
    if (
        !signup_guard(get_interactions, [
            stage,
            params,
            callbackFunc,
            errorFunc,
            repeats || 0,
        ], 'user_id')
    ) {
        return;
    }
    let user_id = getCookie("user_id");
    stage = stage || "favourite";
    if (!params) {
        params = {};
    }
    params[DAVE_SETTINGS.USER_ID] = user_id;
    params[DAVE_SETTINGS.INTERACTION_STAGE_ATTR] = stage;
    let rd = {};
    ajaxRequestWithData(
        "/objects/" + DAVE_SETTINGS.INTERACTION_MODEL,
        "GET",
        params,
        function (data) {
            let products = getCookie('products', {});
            for (let k in data.data) {
                let d = data.data[k];
                if (!products[d.product_id]) {
                    rd[d.product_id] = null;
                    get_product(d.product_id, function (data1) {
                        let rdn = getCookie('favourites', {});
                        if (rdn[d.product_id]) {
                            rdn[d.product_id] = data1;
                            setCookie('favourites', rdn);
                        }
                    });
                } else {
                    rd[d.product_id] = products[d.product_id];
                }
            }
            setCookie('favourites', rd);
            if (callbackFunc) {
                callbackFunc(rd)
            }
        },
        errorFunc
    );
}


//
//  Gets called immediately after signup and can be accessed by cookie name 'adverts'.
//  Format of each advert is the following
//  {
//      "image_url": <url of the image>,
//      "advert_id": < id of the advert>
//      "product_id": <id of the product to load when clicked>",
//      "title": "<main title of the image>",
//      "sub_title": "<sub title of the image>"
//  }
function get_adverts(params, callbackFunc, errorFunc, repeats) {
    if (
        !signup_guard(get_adverts, [
            params,
            callbackFunc,
            errorFunc,
            repeats || 0,
        ])
    ) {
        return;
    }
    params = params || {}
    if (!params["_page_number"]) {
        params["_page_number"] = 1;
        clearCookie('adverts');
    }
    params["device_id"] = getCookie("device_id");
    ajaxRequestWithData(
        "/objects/device_advert",
        "GET",
        params,
        function (data) {
            let adverts = getCookie("adverts") || {};
            for (let k in data.data) {
                let d = data.data[k];
                adverts[d.advert_id] = d;
                preloadImages([d.image_url]);
            }
            setCookie("adverts", adverts);
            if (data.is_last) {
                if (callbackFunc) {
                    callbackFunc(data);
                }
            } else {
                params['_page_number'] = parseInt(params['_page_number']) + 1;
                get_adverts(params, callbackFunc, errorFunc);
            }
        },
        errorFunc
    );
}

// option can be:
// product_name. product_category, design_category, species, structure, finish_type
// params are the values of the other options selected
function get_options(option, params, callbackFunc, errorFunc, repeats) {
    if (
        !signup_guard(get_options, [
            option,
            params,
            callbackFunc,
            errorFunc,
            repeats || 0,
        ])
    ) {
        return;
    }

    params = params || {};
    if (!params["_page_number"]) {
        params["_page_number"] = 1;
    }
    ajaxRequestWithData(
        "/unique/" + DAVE_SETTINGS.PRODUCT_MODEL + "/" + option, //      "finish_type": <finish_type>,
        "GET",
        params,
        callbackFunc,
        errorFunc
    );
}

//
// params like
//  {
//      "product_name": <product_name>,
//      "category_name": <product_category>,
//      "tags": <product search tags>,
//      "genders": <gender>, e.g. male, female, child, uni-sex
//      "sizes": <size> e.g. XL, 44 etc.
//      "search_keywords": <any keyword searched>,
//      "price": "<low price or ''>,<high price or ''>"
//  }
function search_products(params, callbackFunc, errorFunc, repeats) {
    if (
        !signup_guard(search_products, [params, callbackFunc, errorFunc, repeats || 0])
    ) {
        return;
    }
    if (!params) {
        params = {};
    }
    //  params = params || {}
    if (!params["_page_number"]) {
        params["_page_number"] = 1;
    }
    params["_page_size"] = 20;
    ajaxRequestWithData(
        "/objects/" + DAVE_SETTINGS.PRODUCT_MODEL,
        "GET",
        params,
        function (data) {
            let products = getCookie("products") || {};
            for (let k in data.data) {
                let d = data.data[k];
                products[d.product_id] = d;
                preloadImages(d.image);
            }
            setCookie("products", products);
            if (callbackFunc) {
                callbackFunc(data);
            }
        },
        errorFunc
    );
}

//
//
// Use this with QR/Bar code scan or when user clicks on the image
//
function get_product(product_id, callbackFunc, errorFunc, repeats) {
    if (
        !signup_guard(get_product, [
            product_id,
            callbackFunc,
            errorFunc,
            repeats || 0,
        ])
    )
        return;

    let params = {};
    let products = getCookie("products") || {};
    if (products[product_id]) {
        if (callbackFunc) {
            callbackFunc(products[product_id]);
            return;
        }
    }
    ajaxRequestWithData(
        "/object/" + DAVE_SETTINGS.PRODUCT_MODEL + "/" + product_id,
        "GET",
        params,
        function (data) {
            products[data.product_id] = data;
            preloadImages(data.image);
            setCookie('current_product_id', data.product_id);
            setCookie('current_product', data);
            setCookie('matching_categories', data.matching_categories);
            setCookie("products", products);
            if (callbackFunc) {
                callbackFunc(data);
            }
        },
        errorFunc
    );
}

// When a specific design is selected, send the design data to this function to get family of designs
function get_recommended_look(callbackFunc, errorFunc, repeats) {
    if (
        !signup_guard(get_recommended_look, [
            callbackFunc,
            errorFunc,
            repeats || 0,
        ])
    ) {
        return;
    }
    let current_product_id = getCookie('current_product_id');
    let matching_categories = getCookie('matching_categories') || ["pants"];
    if (current_product_id) {
        ajaxRequestWithData(
            "/people_who_also/dave/" + current_product_id + "/shortlisted",
            "GET",
            { "category_name": matching_categories },
            callbackFunc,
            errorFunc
        );
    } else {
        console.error("No product selected currently");
        if (errorFunc) {
            errorFunc();
        }
    }
}

// When a specific design is selected, send the design data to this function to get family of designs
function get_similar_products(callbackFunc, errorFunc, repeats) {
    if (
        !signup_guard(get_similar_products, [
            callbackFunc,
            errorFunc,
            repeats || 0,
        ])
    ) {
        return;
    }
    let current_product_id = getCookie('current_product_id');
    if (current_product_id) {
        ajaxRequestWithData(
            "/similar_products/dave/" + current_product_id,
            "GET",
            {},
            callbackFunc,
            errorFunc
        );
    } else {
        console.error("No product selected currently");
        if (errorFunc) {
            errorFunc();
        }
    }
}

// --------------------------------
// Below this are all internal functions
function iupdate_user(params, callbackFunc, errorFunc) {
    // Used to update session duration and session number
    let user_id = getCookie("user_id");
    //  params = params || {}
    params["_async"] = true;
    ajaxRequestWithData(
        "/iupdate/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id,
        "PATCH",
        JSON.stringify(params),
        callbackFunc,
        errorFunc
    );
}

function create_session(params, callbackFunc, errorFunc) {
    // Used to update session duration and session number
    let user_id = getCookie("user_id");
    let session_id = getCookie("session_id");
    params = params || {}
    if (session_id) {
        ajaxRequestWithData(
            "/object/" + DAVE_SETTINGS.SESSION_MODEL + "/" + session_id,
            "PATCH",
            JSON.stringify({})
        );
    }
    params[DAVE_SETTINGS.SESSION_USER_ID] = user_id;
    params["device_id"] = getCookie("device_id");
    params["store_id"] = getCookie("store_id");
    params["deployment_scenario"] = "kiosk"
    params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;
    params["location_dict"] = {
        "city": getCookie("city"),
        "region_name": getCookie("state"),
        "zipcode": getCookie("pincode"),
        "country": getCookie("country"),
    }
    ajaxRequestWithData(
        "/object/" + DAVE_SETTINGS.SESSION_MODEL,
        "POST",
        JSON.stringify(params),
        function (data) {
            setCookie('session_id', data.session_id)
            if (callbackFunc) {
                callbackFunc(data)
            }
        },
        errorFunc
    );
}

function getCookie(key, _default) {
    let result = window.localStorage.getItem(key);
    return get_value(result, _default);
}

function get_value(value, _default) {
    if (_default === undefined) {
        _default = null;
    }
    if (value === null || value === undefined) {
        return _default;
    }
    try {
        return JSON.parse(value);
    } catch (err) {
        return value;
    }
    return value;
}

function setCookie(key, value, hoursExpire) {
    if (hoursExpire === undefined) {
        hoursExpire = 24;
    }
    if (value === undefined) {
        return;
    }
    if (typeof value == "object") {
        value = JSON.stringify(value);
    }
    if (hoursExpire < 0 || value === null) {
        value = window.localStorage.getItem(key);
        window.localStorage.removeItem(key);
        return value;
    } else {
        window.localStorage.setItem(key, value);
    }
    return value;
}

function clearCookie(key) {
    setCookie(key, null, -24);
}

function Trim(strValue) {
    return strValue.replace(/^\s+|\s+$/g, "");
}

function toTitleCase(str) {
    return str.replace(/_/g, " ").replace(/(?:^|\s)\w/g, function (match) {
        return match.toUpperCase();
    });
}

function generate_random_string(string_length) {
    let random_string = "";
    let random_ascii;
    for (let i = 0; i < string_length; i++) {
        random_ascii = Math.floor(Math.random() * 25 + 97);
        random_string += String.fromCharCode(random_ascii);
    }
    return random_string;
}

function toIdType(str, to_lower) {
    let r = (str || "")
        .replace(/\+/g, "")
        .replace(/\(/g, "")
        .replace(/\)/g, "")
        .replace(/\s/g, "_");
    if (to_lower) {
        return r.toLowerCase();
    }
    return r;
}

function toUrlType(str) {
    return (str || "").replace(/\+/g, "%2B");
}

function toQueryType(str) {
    str = str || "";
    if (str.includes("+")) {
        str = "~" + str.slice(0, str.indexOf("+"));
    }
    if (str.includes(",")) {
        return $.map(str.split(","), function (v) {
            return v.trim();
        });
    }
    return str;
}

function clearCookies() {
    setCookie("authentication", "", -24);
    setCookie("logged_in", "", -24);
    setCookie("applied", "", -24);
    setCookie("shortlisted", "", -24);
    setCookie("shortlisted_per_surface", "", -24);
    setCookie("room_id", "", -24);
    setCookie("product_id", "", -24);
    setCookie("object_id", "", -24);
    setCookie("room_type", "", -24);
    setCookie("viewed", "", -24);
    setCookie("customer_name", "", -24);
    setCookie("customer_email", "", -24);
    setCookie("customer_phone_number", "", -24);
    setCookie("customer_password", "", -24);
    setCookie("customer_city", "", -24);
    setCookie("customer_customer_id", "", -24);
    setCookie("customer_token", "", -24);
}

function get_url_params(qd) {
    qd = qd || {};
    if (window.location.search)
        window.location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
                var s = item.split("="),
                    k = s[0],
                    v = s[1] && decodeURIComponent(s[1]); //  null-coalescing / short-circuit
                //(k in qd) ? qd[k].push(v) : qd[k] = [v]
                (qd[k] = qd[k] || []).push(v); // null-coalescing / short-circuit
            });
    return qd;
}

function to_sequence(data, sequence, indexor) {
    indexor =
        indexor ||
        function (a, s) {
            return s.indexOf(a);
        };
    let op = [];
    for (let s in sequence) {
        let i = indexor(sequence[s], data);
        if (i >= 0) {
            op.push(data[i]);
        }
    }
    return op;
}

function ajaxRequestSync(
    URL,
    METHOD,
    callbackFunc,
    errorFunc,
    async,
    unauthorized,
    HEADERS
) {
    HEADERS = HEADERS || getCookie("authentication");
    if (!unauthorized) {
        unauthorized = function () {
            signup(
                {},
                ajaxRequestSync(
                    URL,
                    METHOD,
                    callbackFunc,
                    errorFunc,
                    async,
                    function () {
                        alert("Could not signup! Please contact customer support!");
                    },
                    HEADERS
                )
            );
        };
    }
    $.ajax({
        url: DAVE_SETTINGS.BASE_URL + URL,
        method: (METHOD || "GET").toUpperCase(),
        dataType: "json",
        contentType: "json",
        async: async || false,
        headers: HEADERS,
        statusCode: {
            401: unauthorized,
            404: unauthorized,
        },
    })
        .done(function (data) {
            let result = data;
            if (callbackFunc) {
                callbackFunc(data);
            }
        })
        .fail(function (err) {
            if (errorFunc) {
                errorFunc(err);
            }
        });
}

function ajaxRequest(
    URL,
    METHOD,
    callbackFunc,
    errorFunc,
    unauthorized,
    HEADERS
) {
    return ajaxRequestSync(
        URL,
        METHOD,
        callbackFunc,
        errorFunc,
        true,
        unauthorized,
        HEADERS
    );
}

function ajaxRequestWithData(
    URL,
    METHOD,
    DATA,
    callbackFunc,
    errorFunc,
    unauthorized,
    HEADERS
) {
    HEADERS = HEADERS || getCookie("authentication");
    var defaultData = "";
    if (DATA) {
        defaultData = DATA;
    }
    if (!unauthorized) {
        unauthorized = function () {
            debugger;
            signup(
                {},
                ajaxRequestWithData(
                    URL,
                    METHOD,
                    DATA,
                    callbackFunc,
                    errorFunc,
                    function () {
                        alert("Could not signup! Please contact customer support!");
                    },
                    HEADERS
                )
            );
        };
    }
    $.ajax({
        url: DAVE_SETTINGS.BASE_URL + URL,
        method: (METHOD || "GET").toUpperCase(),
        dataType: "json",
        contentType: "application/json",
        headers: HEADERS,
        data: defaultData,
        statusCode: {
            401: unauthorized,
            404: unauthorized,
        },
    })
        .done(function (data) {
            if (callbackFunc) {
                callbackFunc(data);
            }
        })
        .fail(function (err) {
            if (errorFunc) {
                errorFunc(err);
            }
        });
}

function signup_guard(func, args) {
    if (!getCookie("authentication")) {
        console.warn(
            "Authentication headers are not set, but trying to run function " +
            func.name
        );
        let repeats = args.slice(-1);
        if (repeats < 3) {
            setTimeout(function () {
                args.push(args.pop() + 1);
                func.apply(args);
            }, 1000 * repeats);
        } else {
            console.error(
                "Stopping trying to run function" +
                func.name +
                " after " +
                repeats +
                "attempts"
            );
        }
        return false;
    }
    return true;
}

function preloadImages(array) {
    if (!preloadImages.list) {
        preloadImages.list = [];
    }
    var list = preloadImages.list;
    if (array) {
        for (var i = 0; i < array.length; i++) {
            var img = new Image();
            img.onload = function () {
                var index = list.indexOf(this);
                if (index !== -1) {
                    // remove image from the array once it's loaded
                    // for memory consumption reasons
                    list.splice(index, 1);
                }
            };
            list.push(img);
            img.src = array[i];
        }
    }
}

$(document).ready(function () {
    if ($("#dave-settings").length) {
        let ds = $("#dave-settings");
        for (let s in DAVE_SETTINGS) {
            if (ds.attr(s)) {
                DAVE_SETTINGS[s] = get_value(
                    ds.attr("data-" + s.toLowerCase()) || ds.attr("data-" + s)
                );
            }
        }
    }
});
