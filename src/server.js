export const apiActions = window.DAVE_SETTINGS
export const apiStreamingSpeechActions = {}

// apiActions.clearCookies()

export const onLoadWithClearData = () => {
  localStorage.clear()
  window.reload()
}

export const onStreamingResultsAvailable = function (data) { //partial data (on-time data)
  console.log('onStreamingResultsAvailable====>>$$$', data);
}

export const onTranscriptionAvailable = function (data) {//This is final data of speech
  let final_data = ''
  if (data.final_text) {
    final_data = data.final_text;
  } else if (data.rec_text) {
    final_data = data.rec_text;
  } else if (data.recognized_speech) {
    final_data = data.recognized_speech;
  }
  console.log('onTranscriptionAvailable====>>$$$', final_data);
  document.getElementById("my_transcription").value = final_data;
  document.getElementById("chat_spinner").style.display = "none";
  // document.getElementById("data-loading").value = false;
};

export const onError = function (data) { // if any error occurred while recording
  console.log('onError====>>$$$', data);
  document.getElementById("my_transcription").value = '';
  document.getElementById("chat_spinner").style.display = "none";
};

export const onConversationResponseAvailable = function (data) { //This one is for [server response on data you have sent]
  console.log('onConversationResponseAvailable====>>$$$', data);
  if (data.conversation_api_response.error || data.conversation_api_response.error === '') {
    document.getElementById("my_transcription").value = '';
    document.getElementById("chat_spinner").style.display = "none";
  }
};

export const onAutoStopRecording = function (data) { //This one is for [server response on data you have sent]
  console.log('onConversationResponseAvailable====>>$$$!@#!#', data);
};

export const onSocketConnect = function (data) { //socket connection check [show mic]
  console.log('onSocketConnect====>>$$$', data);
};

export const onSocketDisConnect = function (data) { //check if socket is disconnected [hide mic]
  console.log('onSocketDisConnect====>>$$$', data);
  document.getElementById("my_transcription").value = '';
};

// apiActions['BASE_URL'] = 'https://test.iamdave.ai'
apiActions['ENTERPRISE_ID'] = 'dave_fashion'
apiActions['SIGNUP_API_KEY'] = "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__"
apiActions['USER_MODEL'] = "customer"
apiActions['USER_ID_ATTR'] = "customer_id"
apiActions['SESSION_MODEL'] = "session"
apiActions['SESSION_USER_ID'] = "customer_id"
apiActions['INTERACTION_USER_ID'] = "customer_id"
apiActions['DEFAULT_USER_DATA'] = { store_id: 'staging' }
apiActions['CONVERSATION_ID'] = "deployment_web"
apiActions['SPEECH_SERVER'] = "https://speech.iamdave.ai/"

console.log('apiActions===>', apiActions)
console.log('window===>', window)