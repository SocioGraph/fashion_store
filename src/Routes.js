import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import history from './history';
import Home from './Screens/Home';
import Product from './Screens/Product';
// import ProductList from './Screens/Product-list';

function Routes() {
    return (
        <Router history={history}>
            <Switch>
                <Route path='/' exact component={Home} />
                <Route path='/product' exact component={Product} />
                {/* <Route path='/product-list' exact component={ProductList} /> */}
            </Switch>
        </Router>
    )
}

export default Routes;
