import Quagga from 'quagga';
import React, { useEffect, useRef, useState } from 'react';
import { useAlert } from 'react-alert';
import Carousel from 'react-elastic-carousel';
import Modal from 'react-modal';
import Images from '../../assets/images';
import BottomTabBar from '../../Components/BottomTabBar';
import FindYourFitModal from '../../Components/FindYourFitModal';
import HeaderTop from '../../Components/HeaderTop';
import Loading from '../../Components/Loading';
import ScanQRCodeModal from '../../Components/ScanQRCodeModal';
import SizeModal from '../../Components/SizeModal';
import history from '../../history';
import { apiActions } from '../../server';
import './home.css';

const {
    AppLogoHeader,
    FindYourSize,
    ScannerLogo,
} = Images;

function Home(props) {

    const [isBarcodeScannerModel, setIsBarcodeScannerModel] = useState(false);
    const [isBarcodeScanner, setIsBarcodeScanner] = useState(false);
    const [adverts, setAdverts] = useState([]);
    const [discountOptions, setDiscountOptions] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isBottomBarLoading, setIsBottomBarLoading] = useState(true);
    const [isDataLoading, setIsDataLoading] = useState(false);
    const [stopStream, setStopStream] = useState(false)
    const [isStopStreamVideo, setIsStopStreamVideo] = useState(true);
    const [isAutoSwipe, setIsAutoSwipe] = useState(true)
    const [isSelectOpenSizeModal, setIsSelectOpenSizeModal] = useState(false);
    const [isOpenSizeModal, setIsOpenSizeModal] = useState(false);
    const [isSizeSwitch, setIsSizeSwitch] = useState(true);
    const mounted = useRef(false);

    const alert = useAlert();

    useEffect(() => {
        // debugger;
        mounted.current = true;
        apiActions.load_dave(
            (res) => { },
            (error) => {
                alert.error(error?.responseJSON?.error);
            }
        );
        apiActions.chat({ "query_type": "auto" },
            (res) => {
                onSetDiscountOptions(res)
                window.localStorage.setItem("query_type", JSON.stringify(res));
                apiActions.ajaxRequestWithData('/objects/advert', "GET", {},
                    (res) => {
                        setAdverts(res.data)
                        setIsLoading(false)
                    },
                    (error) => {
                        alert.error(error?.responseJSON?.error);
                    }
                );
            },
            (error) => {
                alert.error(error?.responseJSON?.error);
            }
        );

        return () => {
            mounted.current = false;
        };
    }, []);


    const onSetDiscountOptions = (value) => {
        if (value.data.options) {
            setDiscountOptions(value.data.options)
        } else {
            setIsBottomBarLoading(false)
        }
    }

    useEffect(() => {
        if (discountOptions.length > 0) {
            setIsBottomBarLoading(false)
        }

    }, [discountOptions]);

    const itemsPerPage = 1
    const carouselRef = useRef(null);
    const totalPages = Math.ceil(adverts && Object.values(adverts).length / itemsPerPage)
    let resetTimeout;


    const closeBarcodeScannerModel = () => {
        setIsBarcodeScannerModel(false);
        setIsBarcodeScanner(false)
    }

    const handleModelScan = () => {
        if (!isDataLoading && !isLoading) {
            setIsBarcodeScannerModel(true)
            setIsBarcodeScanner('')
        }
    }

    const dave_engagement_id = window.localStorage.getItem('dave_engagement_id');
    const dave_user_id = window.localStorage.getItem('dave_user_id');
    const gender = window.localStorage.getItem('gender');
    const current_product = window.localStorage.getItem('current_product');

    const handleClickDiscount = (data) => {
        setIsDataLoading(true)
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST",
            {
                "engagement_id": dave_engagement_id,
                "refresh_cache": true,
                "system_response": "sr_init",
                "customer_response": data.value,
                // "customer_state": data.title
            },
            (response) => {
                window.localStorage.setItem("search_product_title", JSON.stringify(data.value));
                if (response) {
                    document.getElementById("my_transcription").value = '';
                    if (response && response.data && response.data.thumbnails && response.data.thumbnails.length !== 0) {
                        const currentProduct = response?.data?.thumbnails[0]
                        window.localStorage.setItem("current_product", JSON.stringify(currentProduct));
                        response.data.thumbnails.shift();
                        window.localStorage.setItem("similar_products", JSON.stringify(response));
                        if (currentProduct && currentProduct.product_code) {
                            getRecommendedProducts(currentProduct?.product_code)
                        } else {
                            const value = {
                                data: {
                                    thumbnails: []
                                }
                            }
                            window.localStorage.setItem("recommended_products", JSON.stringify(value));
                            history.push({
                                pathname: '/product',
                            });
                        }
                    } else {
                        alert.error('No Data Found');
                        setIsDataLoading(false)
                    }

                }
                setIsDataLoading(false)
            }, (err) => {
                alert.error(err?.responseJSON?.error);
                setIsDataLoading(false)
                document.getElementById("my_transcription").value = '';
            }
        );
    }

    const handleFileSelect = (evt) => {
        setIsDataLoading(true)
        var files = evt.target.files; // FileList object

        var tmpImgURL = URL.createObjectURL(files[0]);
        Quagga.decodeSingle(
            {
                src: tmpImgURL,
                numOfWorkers: 0, // Needs to be 0 when used within node
                locate: true,
                inputStream: {
                    size: 800 // restrict input-size to be 800px in width (long-side)
                },
                decoder: {
                    readers: [
                        "code_128_reader",
                        "code_39_reader",
                        "ean_reader",
                        "ean_8_reader",
                        "code_39_vin_reader",
                        "codabar_reader",
                        "upc_reader",
                        "upc_e_reader",
                        "i2of5_reader"
                    ]
                }
            },
            function (result) {
                if (result) {
                    if (result.codeResult != null) {
                        setIsBarcodeScanner(result.codeResult.code)
                        // getProductFromQRCode(result.codeResult.code)
                        setIsDataLoading(false)
                    } else {
                        setIsDataLoading(false)
                        setIsBarcodeScanner('')
                        alert.error("not detected");
                    }
                } else {
                    setIsDataLoading(false)
                    setIsBarcodeScanner('')
                    alert.error("not detected");
                }
            }
        );
    }

    const getProductFromQRCode = (product_code) => {
        setIsDataLoading(true)
        setIsAutoSwipe(false)
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST",
            {
                "engagement_id": dave_engagement_id,
                "refresh_cache": true,
                "system_response": "sr_init",
                "customer_response": product_code,
            },
            (res) => {
                document.getElementById("my_transcription").value = '';
                if (res && res.data && res.data.thumbnails && res.data.thumbnails.length !== 0) {
                    const currentProduct = res?.data?.thumbnails[0]
                    window.localStorage.setItem("gender", currentProduct?.genders[0]);
                    window.localStorage.setItem("current_product", JSON.stringify(currentProduct));
                    apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
                        "engagement_id": dave_engagement_id,
                        "refresh_cache": true,
                        "system_response": "sr_init",
                        "customer_response": product_code,
                        "customer_state": "cs_similar_product"
                    },
                        (response) => {
                            window.localStorage.setItem("similar_products", JSON.stringify(response));
                            getRecommendedProducts(product_code)
                        },
                        (err) => {
                            alert.error(err?.responseJSON?.error);
                        }
                    )
                } else {
                    alert.error("Product not found!");
                    setIsDataLoading(false)
                    setIsAutoSwipe(true)
                    setIsBarcodeScanner('')
                }
            }, (err) => {
                alert.error(err?.responseJSON?.error);
            }
        );
    }

    const onGetProduct = (product_code) => {
        setIsDataLoading(true)
        setIsAutoSwipe(false)
        apiActions.ajaxRequestWithData(`/object/product/${product_code}`, "GET", {},
            (res) => {
                if (Object.keys(res).length > 0) {
                    window.localStorage.setItem("gender", res?.genders[0]);
                    const objSize = Object.keys(res).length
                    if (objSize !== 0) {
                        window.localStorage.setItem("current_product", JSON.stringify(res));
                        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
                            "engagement_id": dave_engagement_id,
                            "refresh_cache": true,
                            "system_response": "sr_init",
                            "customer_response": res.product_code,
                            "customer_state": "cs_similar_product"
                        },
                            (response) => {
                                window.localStorage.setItem("similar_products", JSON.stringify(response));
                                getRecommendedProducts(res.product_code)

                            },
                            (err) => {
                                alert.error(err?.responseJSON?.error);
                            }
                        )
                    } else {
                        alert.error("Product not found!");
                        setIsDataLoading(false)
                        setIsAutoSwipe(true)
                        setIsBarcodeScanner('')
                    }
                } else {
                    setIsDataLoading(false)
                    setIsAutoSwipe(true)
                    alert.error("Product not found!");
                    setIsBarcodeScanner('')
                }
            },
            (error) => {
                alert.error(error?.responseJSON?.error);
                setIsLoading(false)
                setIsAutoSwipe(true)
            }
        );
    }

    const getRecommendedProducts = (product_code) => {
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
            "engagement_id": dave_engagement_id,
            "refresh_cache": true,
            "system_response": "sr_init",
            "customer_response": product_code,
            "customer_state": "cs_active_nudge_recommendations"
        },
            (resp) => {
                window.localStorage.setItem("recommended_products", JSON.stringify(resp));
                history.push({
                    pathname: '/product',
                });

            },
            (err) => {
                alert.error(err?.responseJSON?.error);
            }
        )
    }

    const onClickAdverts = (item) => {
        window.localStorage.removeItem('search_product_title');
        onGetProduct(item?.product_ids[0]?.toLowerCase())
    }

    const onFindYourSelectedSize = () => {
        setIsSelectOpenSizeModal(!isSelectOpenSizeModal)
    }

    const onFindYourSize = () => {
        setIsSelectOpenSizeModal(false)
        setIsBarcodeScannerModel(false);
        setIsBarcodeScanner(false)
        setIsOpenSizeModal(!isOpenSizeModal)
    }
    const onYesPress = () => {
        setIsSelectOpenSizeModal(false)
        setIsBarcodeScannerModel(false);
        setIsBarcodeScanner(false)
        getProductFromQRCode(isBarcodeScanner)
    }

    const onSetIsSizeSwitch = (value) => {
        setIsSizeSwitch(value)
    }

    const setIsLoadingValue = (value) => {
        setIsLoading(value)
    }

    const customBlurModalStyles = {
        overlay: {
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'transparent',
            backdropFilter: 'blur(8px)'
        },
        content: {
            position: 'absolute',
            top: 40,
            left: 40,
            right: 40,
            bottom: 40,
            border: '1px solid #ccc',
            background: 'transparent',
            overflow: 'auto',
            WebkitOverflowScrolling: 'touch',
            borderRadius: '4px',
            outline: 'none',
            padding: '20px',
            height: "100%",
            width: '100%',
        }
    };

    const onGetUpdatedData = (data) => {
        return
    }

    return (
        <div className="home-screen">
            <div style={{ display: 'none' }} id="chat_spinner">
                <div className="loading-home-screen-view">
                    <Loading />
                </div>
            </div>
            <img src={AppLogoHeader} className="dave-ai-logo-header" />
            {isLoading || isDataLoading ? (
                <div className="loading-home-screen-view">
                    <Loading />
                </div>
            ) : <></>}
            <div>
                <Carousel
                    ref={carouselRef}
                    enableAutoPlay={isAutoSwipe}
                    showArrows={false}
                    itemPadding={[0, 20]}
                    outerSpacing={60}
                    autoPlaySpeed={20000}
                    itemsToShow={itemsPerPage}
                    onNextEnd={({ index }) => {
                        clearTimeout(resetTimeout)
                        if (index + 1 === totalPages) {
                            if (carouselRef?.current?.goTo) {
                                resetTimeout = setTimeout(() => {
                                    if (carouselRef?.current?.goTo) {
                                        carouselRef.current.goTo(0)
                                    }
                                }, 3000)
                            }
                        }
                    }}
                    renderPagination={({ pages, activePage, onClick }) => {
                        return (
                            <div className="active-page-dots-view">
                                {pages.map((page, index) => {
                                    const isActivePage = activePage === page
                                    return (
                                        <div
                                            key={index}
                                            className={`${isActivePage ? 'active-page-dots' : 'inactive-page-dots'}`}
                                            onClick={() => onClick(page)}
                                            active={isActivePage}
                                        />
                                    )
                                })}
                            </div>
                        )
                    }}
                >
                    {adverts.map((item, index) => {
                        return (
                            <div
                                className="carousel-card-main"
                                key={index} onClick={() => {
                                    onClickAdverts(item)
                                }}>
                                <div
                                    className="justify-content-center display-flex width-100"
                                    style={{
                                        height: '40vh',
                                    }}
                                >
                                    <img
                                        src={item.image_url.slice(0, item.image_url.indexOf(",https://"))}
                                        className="home-slider-image"
                                    />
                                </div>
                                <div
                                    className="justify-content-space-between align-self-center align-items-center display-flex flex-direction-column"
                                    style={{
                                        height: '36vh',
                                    }}>
                                    <div style={{
                                        backgroundColor: '#fff',
                                        width: '90%',
                                    }}>
                                        <p className="home-top-card-title">{item.title}</p>
                                        <p className="home-top-card-offer">{item.advert_name}</p>
                                    </div>
                                    <div className="tc-apply-view">
                                        <p className="tc-apply-text">*T&C Apply</p>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </Carousel>

                <div className="scan-modal">
                    {isBarcodeScannerModel &&
                        <div className="scan-qr-close-modal-view">
                            <HeaderTop renderScreen="Home" onBack={closeBarcodeScannerModel} />
                        </div>
                    }
                    <ScanQRCodeModal
                        isBarcodeScannerModel={isBarcodeScannerModel}
                        closeBarcodeScannerModel={closeBarcodeScannerModel}
                        stopStream={stopStream}
                        setIsBarcodeScanner={setIsBarcodeScanner}
                        setStopStream={setStopStream}
                        handleFileSelect={handleFileSelect}
                        isBarcodeScanner={isBarcodeScanner}
                        onFindYourSelectedSize={onFindYourSelectedSize}
                        getProductFromQRCode={getProductFromQRCode}
                        setIsLoadingValue={setIsLoadingValue}
                        onStopQRScanner={(e) => { setIsStopStreamVideo(e) }}
                        isShowQRScanner={isStopStreamVideo}
                    />
                </div>
                <SizeModal
                    isSelectOpenSizeModal={isSelectOpenSizeModal}
                    onFindYourSelectedSize={onFindYourSelectedSize}
                    onYesPress={onYesPress}
                    onFindYourSize={onFindYourSize}
                    isBarcodeScanner={isBarcodeScanner}
                />
                <Modal
                    isOpen={isOpenSizeModal}
                    onRequestClose={onFindYourSize}
                    contentLabel="Example Modal"
                    style={customBlurModalStyles}
                >
                    <FindYourFitModal
                        onSetIsSizeSwitch={(e) => onSetIsSizeSwitch(e)}
                        isSizeSwitch={isSizeSwitch}
                        onFindYourSize={onFindYourSize}
                        stepNumber={1}
                        isGoBack={false}
                        isShowGenderSelection={gender && current_product ? false : true}
                        setIsLoadingValue={setIsLoadingValue}
                    />
                </Modal>
                <div className="left right bottom position-fixed">
                    {isBottomBarLoading ?
                        <div className="justify-content-center display-flex align-items-center" style={{ height: 217 }}>
                            <Loading />
                        </div>
                        :
                        <>
                            <div className="display-flex justify-content-space-evenly">
                                <div className="scanner-qr-code-view" onClick={() => handleModelScan()}>
                                    <img src={ScannerLogo} className="scanner-logo" />
                                    <p className="scanner-title">Scan product</p>
                                </div>
                                <div className="scanner-qr-code-view" onClick={() => onFindYourSize()}>
                                    <img src={FindYourSize} className="find-your-size-icon" />
                                    <p className="scanner-title">Find Your Size</p>
                                </div>
                            </div>
                            <BottomTabBar
                                screen="Home"
                                isBarcodeScannerModel={isBarcodeScannerModel}
                                discountOptions={discountOptions}
                                handleClickDiscount={(e) => handleClickDiscount(e)}
                                onManageLoading={(e) => setIsLoadingValue(e)}
                                onGetUpdatedData={(e) => onGetUpdatedData(e)}
                                onStopQRScanner={(e) => { setIsStopStreamVideo(e) }}
                                isShowQRScanner={isStopStreamVideo}
                            />
                        </>
                    }
                </div>
            </div>
        </div>
    );
}

export default Home;
