import React, { useEffect, useState } from 'react';
import history from '../../history';
import { apiActions } from '../../server';
import './login.css';

const Login = () => {
    const initialValues = { email: "", password: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    const submitForm = () => {
        apiActions.login(
            formValues.email,
            formValues.password,
            (res) => {
                if (res?.api_key) {
                    apiActions.get_adverts(
                        {},
                        (response) => {
                            if (response) {
                                history.push({
                                    pathname: '/home',
                                });
                            }
                        },
                        (error) => {
                            alert.error(error.responseJSON.error);
                        }
                    );
                }
            },
            (error) => {
                alert.error(error.responseJSON.error);
            }
        );
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        window.localStorage.clear()
        apiActions.clearCookies()
        setFormErrors(validate(formValues));
        setIsSubmitting(true);
    };

    const validate = (values) => {
        let errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.email) {
            errors.email = "Required!";
        } else if (!regex.test(values.email)) {
            errors.email = "Invalid email format";
        }
        if (!values.password) {
            errors.password = "Required!";
        } else if (values.password.length < 6) {
            errors.password = "Password must be more than 6 characters";
        }
        return errors;
    };

    useEffect(() => {
        // if (Object.keys(formErrors).length === 0 && isSubmitting) {
        //     submitForm();
        // }
        apiActions.signup(
            (res) => {
                // history.replace('home', null);
            },
            (error) => {
                alert.error(error.responseJSON.error);
            }
        );
    }, []);
    return (
        <div className="login-screen">

            <form style={{ width: '100%', display: 'flex', justifyContent: 'center', }} onSubmit={handleSubmit} noValidate
                method="post">
                <div className="login-card" style={{ borderRadius: '2%' }}>
                    <div className="login-title">
                        Sign In
                    </div>
                    <div className="form-group">
                        <label className="login-input-label">Email address</label>
                        <input type="email" name="email"
                            onChange={handleChange}
                            className="login-input" placeholder="Enter email" />
                    </div>
                    {formErrors.email && (
                        <span className="error">{formErrors.email}</span>
                    )}

                    <div className="form-group">
                        <label className="login-input-label">Password</label>
                        <input type="password" name="password"
                            onChange={handleChange}
                            className="login-input" placeholder="Enter password" />
                    </div>
                    {formErrors.password && (
                        <span className="error">{formErrors.password}</span>
                    )}

                    <button type="submit" className="btn  btn-block button-submit">Submit</button>
                </div>
            </form>
        </div>
    );
}

export default Login;
