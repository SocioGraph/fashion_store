import GoogleMapReact from 'google-map-react';
import React, { useEffect, useRef, useState } from 'react';
import { useAlert } from 'react-alert';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import Carousel, { consts } from 'react-elastic-carousel';
import Modal from 'react-modal';
import Images from '../../assets/images';
import BottomTabBar from '../../Components/BottomTabBar';
import DropdownSelect from '../../Components/DropdownSelect';
import FindYourFitModal from '../../Components/FindYourFitModal';
import HeaderTop from '../../Components/HeaderTop';
import Loading from '../../Components/Loading';
import Marker from '../../Components/Marker';
import ProductCardSwiper from '../../Components/ProductCardSwiper';
import ShareQRCodeModal from '../../Components/ShareQRCodeModal';
import {
    PriceRangeSelector,
    ProductCardData,
    productCategoryList,
    sizeSelector
} from '../../data';
import history from '../../history';
import { apiActions } from '../../server';
import './product.css';

const {
    FindYourSize,
    LikeIcon,
    LikeBorderIcon,
    SwipeArrow,
    ViewInformation,
    WarningIcon,
    IconSend,
} = Images

function Product(props) {
    const dave_engagement_id = window.localStorage.getItem('dave_engagement_id');
    const dave_user_id = window.localStorage.getItem('dave_user_id');
    const gender = window.localStorage.getItem('gender');
    const my_size = window.localStorage.getItem('my_size');
    const [selectYourSize, setSelectYourSize] = useState();
    const [selectPriceRange, setSelectPriceRange] = useState('Low To High');
    const [selectColour, setSelectColour] = useState(null);
    const [selectStyle, setSelectStyle] = useState(null);
    const [selectFit, setSelectFit] = useState(null);
    const [isShowPent, setIsShowPent] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [isBarcodeModel, setIsBarcodeModel] = useState(false);
    const [mobileNumber, setMobileNumber] = useState(null);
    const [errorsPhoneNumber, setErrorsPhoneNumber] = useState('');
    const [timeLeft, setTimeLeft] = useState(100);
    // const [timeLeft, setTimeLeft] = useState(99999999999999999999999999999999999999999999999999);
    const [timeLogoutLeft, setTimeLogoutLeft] = useState(null);
    const [currentProduct, setCurrentProduct] = useState(null);
    const [similarProducts, setSimilarProducts] = useState(null);
    const [recommendedLookProducts, setRecommendedLookProducts] = useState({});
    const [productsBarcode, setProductsBarcode] = useState(null);
    const [productsInformation, setProductsInformation] = useState(false);
    const [isCustomizeProduct, setIsCustomizeProduct] = useState(false);
    const [productCategory, setProductCategory] = useState("");
    const [selectedProductCategory, setSelectedProductCategory] = useState("Recommended Style");
    const [isFavoritesProduct, setIsFavoritesProduct] = useState(false);
    const [productCardList, setProductCardList] = useState([]);
    const [productSlider, setProductSlider] = useState([]);
    const [isOpenSizeModal, setIsOpenSizeModal] = useState(false);
    const [isSizeSwitch, setIsSizeSwitch] = useState(true);
    const [recommendedProductSlider, setRecommendedProductSlider] = useState([]);
    const [productSliderLoading, setProductSliderLoading] = useState(true);
    const [selectRecommendedProduct, setSelectRecommendedProduct] = useState({});
    const [isProductInFavorite, setIsProductInFavorite] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [isSimilarProductsLoading, setIsSimilarProductsLoading] = useState(true);
    const [isRecommendedLookProductsLoading, setIsRecommendedLookProductsLoading] = useState(true);
    const [isBottomBarLoading, setIsBottomBarLoading] = useState(true)
    const [discountOptions, setDiscountOptions] = useState([]);
    const [isShowSelectedProjectSlider, setIsShowSelectedProjectSlider] = useState(false);
    const [favoriteProductList, setFavoriteProductList] = useState([]);
    const [allSubCategoryList, setAllSubCategoryList] = useState([]);
    const [allColourList, setAllColourList] = useState([]);
    const [allBrandNameList, setAllBrandNameList] = useState([]);
    const [isSetFilter, setIsSetFilter] = useState(false);
    const [mapCenter, setMapCenter] = useState({
        lat: 21.170240,
        lng: 72.831062
    });
    const [places, setPlaces] = useState([{
        id: 1,
        name: 'surat',
        geometry: {
            location: {
                lat: 21.170240,
                lng: 72.831062
            }
        }
    }])

    const alert = useAlert();

    const handleScroll = () => {
        setTimeLeft(100)
        setTimeLogoutLeft(null)
    };

    useEffect(() => {
        const setFromEvent = () => {
            setTimeLeft(100)
            setTimeLogoutLeft(null)
        }
        window.addEventListener("mousemove", setFromEvent);
    }, []);

    useEffect(() => {
        onGetLocalData()
    }, []);


    const onGetLocalData = () => {
        console.log('onGetLocalData is calling!#!@!#!#!!#!#!')
        const current_product = JSON.parse(window.localStorage.getItem('current_product'));
        const similar_products = JSON.parse(window.localStorage.getItem('similar_products'));
        const recommended_products = JSON.parse(window.localStorage.getItem('recommended_products'));
        // setIsFavoritesProduct(false)
        setIsCustomizeProduct(false)
        setProductsInformation(false)
        const query_type = JSON.parse(window.localStorage.getItem('query_type'));
        if (current_product) {
            const objSize = Object.keys(current_product).length
            if (objSize !== 0) {
                setCurrentProduct(current_product)
                onSetMySize(current_product)
            }
        }
        if (similar_products) {
            setSimilarProducts(similar_products)
            getAllSubCategoryList(similar_products.data.thumbnails)
        }
        if (recommended_products) {
            setRecommendedLookProducts(recommended_products)

        }
        onSetDiscountOptions(query_type)
    }

    const getAllSubCategoryList = (data) => {
        const uniqueSubCategoryList = [...new Set(data.map(item => item.sub_category))];
        const uniqueColourList = [...new Set(data.map(item => item.colour))];
        const uniqueBrandNameList = [...new Set(data.map(item => item.brand_name))];
        onCreateArrayFormatting(uniqueSubCategoryList, setAllSubCategoryList)
        onCreateArrayFormatting(uniqueColourList, setAllColourList)
        onCreateArrayFormatting(uniqueBrandNameList, setAllBrandNameList)
    }

    const onCreateArrayFormatting = (data, setIn) => {
        let array = data
        for (let i = 0; i < array.length; i++) {
            if (array[i]) {
                array[i] = {
                    id: i,
                    title: array[i],
                    selected: false,
                    key: array[i],
                }
            }
            if (i === array.length - 1) {
                if (array) {
                    var filtered = array.filter(Boolean);
                    setIn(filtered)
                } else {
                    setIn([])
                }
            }
        }

    }

    const onSetMySize = (data) => {
        let isSizeAdded = false
        if (data.sizes) {
            for (let i = 0; i < data.sizes.length; i++) {
                if (data.sizes[i] === my_size) {
                    isSizeAdded = true
                }
                if (i === data.sizes.length - 1) {
                    if (isSizeAdded) {
                        setSelectYourSize(my_size)
                    } else {
                        setSelectYourSize(null)
                    }
                }
            }
        }
    }

    const onSetDiscountOptions = (value) => {
        if (value.data.options) {
            setDiscountOptions(value.data.options)
        } else {
            setIsBottomBarLoading(false)
        }
    }

    useEffect(() => {
        if (recommendedLookProducts) {
            setIsRecommendedLookProductsLoading(false)
        }
        if (similarProducts) {
            setIsSimilarProductsLoading(false)
        }
    }, [recommendedLookProducts, similarProducts]);

    useEffect(() => {
        if (currentProduct) {
            setProductSlider(currentProduct.image)
            setTimeout(() => {
                setProductSliderLoading(false);
            }, 2000);

            setIsLoading(false)
        }
    }, [currentProduct]);

    useEffect(() => {
        if (Object.keys(discountOptions).length > 0) {
            setIsBottomBarLoading(false)
        }
    }, [discountOptions]);

    useEffect(() => {
        if (favoriteProductList?.length > 0) {
            getProductInFavorite()
        }
    }, [favoriteProductList, isShowSelectedProjectSlider]);

    useEffect(() => {
        const objSize = Object.keys(selectRecommendedProduct).length
        if (objSize !== 0 && recommendedProductSlider.length === 0) {
            if (selectRecommendedProduct) {
                setRecommendedProductSlider(selectRecommendedProduct.image)
                setTimeout(() => {
                    setProductSliderLoading(false);
                }, 2000);
                setIsLoading(false)
            }
            setIsShowPent(true);
            if (selectRecommendedProduct.image[0] && selectRecommendedProduct.image[0] !== 'None') {
                setSelectedProduct(selectRecommendedProduct.image[0])
            } else {
                setSelectedProduct(recommendedProductSlider[0])
            }
        }
    }, [selectRecommendedProduct]);

    const getProductInFavorite = () => {
        let isProductFavorite = null
        if (favoriteProductList) {
            for (let i = 0; i < favoriteProductList.length; i++) {
                if (isShowSelectedProjectSlider) {
                    if (favoriteProductList[i].product_id === selectRecommendedProduct?.product_id) {
                        isProductFavorite = true
                    }
                } else {
                    if (favoriteProductList[i].product_id === currentProduct?.product_id) {
                        isProductFavorite = true
                    }
                }

                if (favoriteProductList.length - 1 === i) {
                    if (isProductFavorite) {
                        setIsProductInFavorite(true)
                    }
                    else {
                        setIsProductInFavorite(false)
                    }
                }
            }
        }
    }


    useEffect(() => {
        if (isShowSelectedProjectSlider ? selectRecommendedProduct : currentProduct) {
            getFavoriteProduct()
        }
    }, [selectRecommendedProduct, currentProduct]);

    useEffect(() => {
        const items = ProductCardData
        const n = 8
        const result = new Array(Math.ceil(items.length / n))
            .fill()
            .map(_ => items.splice(0, n))
        setProductCardList(() => result)
    }, []);

    useEffect(() => {
        if (!timeLeft) {
            if (timeLogoutLeft === null) {
                setTimeLogoutLeft(10)
            }
            return confirmAlert({
                customUI: ({ onClose }) => {
                    if (timeLogoutLeft === 0) {
                        onClose()
                        localStorage.clear()
                        apiActions.clearCookies()
                        cleanLocalData()
                        history.push({
                            pathname: '/',
                            state: {
                                isSignup: true,
                            }
                        });
                    }
                    return (
                        <div className='custom-ui'>
                            <div>
                                <p className="react-confirm-alert-title">Are you still around?</p>
                                <p className="react-confirm-alert-message">Closing Session in <span
                                    className="react-confirm-alert-timer">{timeLogoutLeft} Sec</span></p>
                            </div>
                            <div className="react-confirm-alert-button">
                                {/* <button className="react-confirm-alert-no-button" onClick={() => { onNoButtonPress() }}>No</button> */}
                                <button className="react-confirm-alert-yes-button" onClick={() => {
                                    onClose()
                                }}>Yes, I am still here.
                                </button>
                            </div>
                        </div>
                    );
                }
            });

        }

        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 1);
        }, 1000);
        return () => clearInterval(intervalId);
    }, [timeLeft, timeLogoutLeft]);

    useEffect(() => {
        if (!timeLogoutLeft) return;
        const intervalId = setInterval(() => {
            setTimeLogoutLeft(timeLogoutLeft - 1);
        }, 1000);
        return () => clearInterval(intervalId);
    }, [timeLogoutLeft]);


    const cleanLocalData = () => {
        window.localStorage.removeItem('recommended_products');
        window.localStorage.removeItem('current_product');
        window.localStorage.removeItem('similar_products');
        window.localStorage.removeItem('my_size');
        window.localStorage.removeItem('search_product_title');
        document.getElementById("my_transcription").value = '';
    }


    const getFavoriteProduct = () => {
        let data = isShowSelectedProjectSlider ? selectRecommendedProduct : currentProduct
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
            "engagement_id": dave_engagement_id,
            "refresh_cache": true,
            "system_response": "sr_init",
            "customer_response": data.product_id,
            "customer_state": "cs_show_favourite"
        },
            (resp) => {
                if (resp?.data?.favourite) {
                    setFavoriteProductList(resp.data.favourite)
                    getProductInFavorite()
                }
            },
            (err) => {
                alert.error(err?.responseJSON?.error);
            }
        )
    }

    const handleChangeSize = (value) => {
        setSelectYourSize(value)
        window.localStorage.setItem("my_size", value);
    }
    const handleChangePriceRange = (value) => {
        setSelectPriceRange(value)
    }
    const handleChangeColour = (value) => {
        setSelectColour(value)
    }
    const handleChangeStyle = (value) => {
        setSelectStyle(value)
    }
    const handleChangeFit = (value) => {
        setSelectFit(value)
    }

    const onClearFilters = () => {
        setSelectYourSize(null)
        setSelectPriceRange(null)
        setSelectColour(null)
        setSelectStyle(null)
        setSelectFit(null)
    }

    const onChangeFilter = () => {
        setIsLoading(true)
        setIsSimilarProductsLoading(true)
        setIsRecommendedLookProductsLoading(true)
        const data = {
            colour: selectColour ? selectColour : '',
            brand: selectStyle ? selectStyle : '',
            sub_category: selectFit ? selectFit : '',
            order: selectPriceRange === 'High to Low' ? false : true,
            gender: gender,
            size: my_size ? my_size : '',

        }
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST",
            {
                "engagement_id": dave_engagement_id,
                "refresh_cache": true,
                "system_response": "sr_init",
                "customer_response": JSON.stringify(data),
                "customer_state": "cs_category"
            },
            (response) => {
                if (response?.data?.thumbnails) {
                    setIsLoading(false);
                    document.getElementById("my_transcription").value = '';
                    if (response && response.data && response.data.thumbnails && response.data.thumbnails.length !== 0) {
                        const currentProduct = response?.data?.thumbnails[0]
                        window.localStorage.setItem("current_product", JSON.stringify(currentProduct));
                        response.data.thumbnails.shift();
                        window.localStorage.setItem("similar_products", JSON.stringify(response));
                        if (currentProduct && currentProduct.product_code) {
                            getRecommendedProducts(currentProduct?.product_code)
                        } else {
                            const value = {
                                data: {
                                    thumbnails: []
                                }
                            }
                            window.localStorage.setItem("recommended_products", JSON.stringify(value));
                            onGetLocalData()
                            history.push({
                                pathname: '/product',
                            });
                        }
                    } else {
                        alert.error('No Data Found');
                        onClearFilters()
                        onGetLocalData()
                        setIsLoading(false)
                        setIsSimilarProductsLoading(false)
                        setIsRecommendedLookProductsLoading(false)
                    }
                }
                if (response?.data?.error) {
                    onClearFilters()
                    alert.error(response?.data?.error);
                    setIsLoading(false)
                    setIsSimilarProductsLoading(false)
                    setIsRecommendedLookProductsLoading(false)
                }
            },
            (error) => {
                onClearFilters()
                alert.error(error.responseJSON.error);
                setIsLoading(false)
                setIsSimilarProductsLoading(false)
                setIsRecommendedLookProductsLoading(false)
            });
    }

    const myArrow = ({ type, onClick, isEdge }) => {
        const pointer = type === consts.PREV ?
            <img src={SwipeArrow} className="swipe-arrow-left" onClick={onClick} />
            :
            <img src={SwipeArrow} className="swipe-arrow-right" onClick={onClick} />
        return (
            <div disabled={isEdge} style={{ display: 'flex', alignItems: 'center' }}>
                {pointer}
            </div>
        )
    }

    const onSelectPent = (item) => {
        setRecommendedProductSlider([])
        setSelectRecommendedProduct(item)
        getProductInFavorite()
    }

    const afterOpenModal = () => {
        // references are now sync'd and can be accessed.
    }

    const closeModal = () => {
        setIsBarcodeModel(false);
    }

    const handleClickDiscount = (data) => {
        const value = {
            data: {
                thumbnails: []
            }
        }
        onClearFilter()
        setIsFavoritesProduct(false)
        setIsCustomizeProduct(false)
        setProductsInformation(false)
        setIsLoading(true)
        setIsSimilarProductsLoading(true)
        setIsRecommendedLookProductsLoading(true)
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST",
            {
                "engagement_id": dave_engagement_id,
                "refresh_cache": true,
                "system_response": "sr_init",
                "customer_response": data.value,
                // "customer_state": data.title
            },
            (response) => {

                window.localStorage.setItem("search_product_title", JSON.stringify(data.value));
                if (response?.data?.thumbnails) {
                    // window.localStorage.setItem("product-list", JSON.stringify(response));
                    document.getElementById("my_transcription").value = '';
                    if (response && response.data && response.data.thumbnails && response.data.thumbnails.length !== 0) {
                        const currentProduct = response?.data?.thumbnails[0]
                        window.localStorage.setItem("current_product", JSON.stringify(currentProduct));
                        response.data.thumbnails.shift();
                        window.localStorage.setItem("similar_products", JSON.stringify(response));
                        setIsSimilarProductsLoading(false)
                        setSimilarProducts(response)
                        getAllSubCategoryList(response.data.thumbnails)
                        if (currentProduct && currentProduct.product_code) {
                            setCurrentProduct(currentProduct)
                            onSetMySize(currentProduct)
                            getRecommendedProducts(currentProduct?.product_code)
                        } else {
                            window.localStorage.setItem("recommended_products", JSON.stringify(value));
                            history.push({
                                pathname: '/product',
                            });
                        }
                    } else {
                        alert.error('No Data Found');
                        onClearFilters()
                        setIsLoading(false)
                        setIsSimilarProductsLoading(false)
                        setIsRecommendedLookProductsLoading(false)
                    }

                }
                if (response?.data?.error) {
                    alert.error(response?.data?.error);
                    onClearFilters()
                    setIsLoading(false)
                    setIsSimilarProductsLoading(false)
                    setIsRecommendedLookProductsLoading(false)
                }
                setIsLoading(false)
            }, (err) => {
                alert.error(err?.responseJSON?.error);
                onClearFilters()
                setIsLoading(false)
                setIsSimilarProductsLoading(false)
                setIsRecommendedLookProductsLoading(false)
                document.getElementById("my_transcription").value = '';
            }
        );
    }

    const customBlurModalStyles = {
        overlay: {
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'transparent',
            backdropFilter: 'blur(8px)'
        },
        content: {
            position: 'absolute',
            top: 40,
            left: 40,
            right: 40,
            bottom: 40,
            border: '1px solid #ccc',
            background: 'transparent',
            overflow: 'auto',
            WebkitOverflowScrolling: 'touch',
            borderRadius: '4px',
            outline: 'none',
            padding: '20px',
            height: "100%",
            width: '100%',
        }
    };

    const onSendMobile = () => {
        if (mobileNumber) {
            if (mobileNumber.match(/^[0-9\b]+$/)) {
                if (!mobileNumber) {
                    setErrorsPhoneNumber("Please enter your phone number.")
                } else if (mobileNumber.length < 10) {
                    setErrorsPhoneNumber("Please put 10 digit phone number")
                } else if (mobileNumber.length > 10) {
                    setErrorsPhoneNumber("Phone number must be at least 10 digit")
                } else {
                    setErrorsPhoneNumber("")
                }
            } else {
                setErrorsPhoneNumber("Invalid Phone number")
            }
        } else {
            setErrorsPhoneNumber("Please enter your phone number.")
        }
    }

    const onSelectedSimilarProducts = (index, item) => {
        const similar_products = JSON.parse(window.localStorage.getItem('similar_products'));
        onClearFilter()
        setIsRecommendedLookProductsLoading(true)
        setSelectedProduct(false)
        setIsCustomizeProduct(false)
        setIsShowPent(false)
        setSelectRecommendedProduct({})
        onSetCurrentProductSlider()
        apiActions.ajaxRequestWithData(`/object/product/${item.product_id}`, "GET", {},
            (res) => {
                if (res) {
                    window.localStorage.setItem("gender", res?.genders[0]);
                    const objSize = Object.keys(res).length
                    if (objSize !== 0) {
                        similarProducts.data.thumbnails[index] = currentProduct;
                        setSimilarProducts(similarProducts)
                        getAllSubCategoryList(similarProducts.data.thumbnails)
                        window.localStorage.setItem("current_product", JSON.stringify(res));
                        window.localStorage.setItem("similar_products", JSON.stringify(similar_products));
                        setCurrentProduct(res)
                        onSetMySize(res)
                        getRecommendedProducts(item.product_code)
                    } else {
                        setIsRecommendedLookProductsLoading(false)
                    }
                }
            },
            (err) => {
                alert.error(err?.responseJSON?.error);
                setIsRecommendedLookProductsLoading(false)
                setProductSliderLoading(false)
                setIsProductInFavorite(false)
            }
        )
        setProductSliderLoading(false)
        setIsProductInFavorite(false)
        setProductSlider([])
    }

    const getRecommendedProducts = (product_id) => {
        setRecommendedLookProducts({ data: { thumbnails: [] } })
        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
            "engagement_id": dave_engagement_id,
            "refresh_cache": true,
            "system_response": "sr_init",
            "customer_response": product_id,
            "customer_state": "cs_active_nudge_recommendations"
        },
            (resp) => {
                if (resp?.data?.thumbnails) {
                    window.localStorage.setItem("recommended_products", JSON.stringify(resp));
                    setRecommendedLookProducts(resp)
                    setIsRecommendedLookProductsLoading(false)
                    setProductSliderLoading(false)
                    setIsProductInFavorite(false)
                    onGetLocalData()
                }
                if (resp?.data?.error) {
                    window.localStorage.setItem("recommended_products", JSON.stringify(resp));
                    setRecommendedLookProducts(resp)
                    alert.error(resp?.data?.error);
                    setIsRecommendedLookProductsLoading(false)
                    setProductSliderLoading(false)
                    setIsProductInFavorite(false)
                    onGetLocalData()
                }
            },
            (err) => {
                alert.error(err?.responseJSON?.error);
                setIsRecommendedLookProductsLoading(false)
                setProductSliderLoading(false)
                setIsProductInFavorite(false)
            }
        )
    }

    const itemsPerPage = 2
    const productCarouselRef = useRef(null);
    const carouselRef2 = useRef(null);
    const carouselRef3 = useRef(null);
    const totalPages = Math.ceil(productSlider?.length / itemsPerPage)
    let resetTimeout;

    const onProductFavoritesPress = () => {
        setIsFavoritesProduct(!isFavoritesProduct)
        if (favoriteProductList.length) {
            window.localStorage.setItem("old_current_product", JSON.stringify(currentProduct));
            window.localStorage.setItem("current_product", JSON.stringify(favoriteProductList[0]));
            window.localStorage.setItem("selected_product", JSON.stringify(selectedProduct));
            setSelectedProduct(false)
            setIsShowSelectedProjectSlider(false)
            setIsShowPent(false)
            setFavoriteProductList(favoriteProductList)
            setCurrentProduct(favoriteProductList[0])
            onSetMySize(favoriteProductList[0])
        }
    }

    const addToFavorite = (data) => {
        setIsLoading(true)
        let matchValue = false;
        let addNewData = false;
        if (favoriteProductList && favoriteProductList?.length !== 0) {
            for (let i = 0; i < favoriteProductList.length; i++) {
                if (favoriteProductList[i].product_id === data.product_id && !addNewData) {
                    matchValue = true
                    apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
                        "engagement_id": dave_engagement_id,
                        "refresh_cache": true,
                        "system_response": "sr_init",
                        "customer_response": data.product_id,
                        "customer_state": "cs_remove_favourite"
                    },
                        (resp) => {
                            favoriteProductList.splice(i, 1);
                            window.localStorage.setItem("favorite_product", JSON.stringify(favoriteProductList));
                            alert.success("Item removed to favorites...");
                            setFavoriteProductList(favoriteProductList)
                            setIsProductInFavorite(false)
                            setIsProductInFavorite(false)
                        },
                        (err) => {
                            setIsProductInFavorite(false)
                            alert.error(err?.responseJSON?.error);
                        }
                    )
                    setIsLoading(false)
                }
                if (favoriteProductList.length - 1 === i) {
                    if (!matchValue && !addNewData) {
                        apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
                            "engagement_id": dave_engagement_id,
                            "refresh_cache": true,
                            "system_response": "sr_init",
                            "customer_response": data.product_id,
                            "customer_state": "cs_favourite"
                        },
                            (resp) => {
                                favoriteProductList.push(data)
                                alert.success("Item added to favorites...");
                                setFavoriteProductList(favoriteProductList)
                                window.localStorage.setItem("favorite_product", JSON.stringify(favoriteProductList));
                                addNewData = true
                                setIsProductInFavorite(true)
                            },
                            (err) => {
                                alert.error(err?.responseJSON?.error);
                                setIsProductInFavorite(true)
                            }
                        )
                        setIsLoading(false)
                    }
                }
            }
        } else {
            apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
                "engagement_id": dave_engagement_id,
                "refresh_cache": true,
                "system_response": "sr_init",
                "customer_response": data.product_id,
                "customer_state": "cs_favourite"
            },
                (resp) => {
                    favoriteProductList.push(data)
                    alert.success("Item added to favorites...");
                    setFavoriteProductList(favoriteProductList)
                    window.localStorage.setItem("favorite_product", JSON.stringify(favoriteProductList));
                    addNewData = true
                    setIsProductInFavorite(true)
                },
                (err) => {
                    setIsProductInFavorite(true)
                    alert.error(err?.responseJSON?.error);
                }
            )
            setIsLoading(false)
        }
    }

    const onHeaderBackPress = () => {
        const old_current_product = JSON.parse(window.localStorage.getItem('old_current_product'));
        const current_product = JSON.parse(window.localStorage.getItem('current_product'));
        const favorite_product = JSON.parse(window.localStorage.getItem('favorite_product'));
        const selected_product = JSON.parse(window.localStorage.getItem('selected_product'));
        if (Object.keys(current_product).length !== 0) {
            if (old_current_product && (Object.keys(old_current_product).length !== 0)) {
                window.localStorage.setItem("old_current_product", '{}');
                window.localStorage.setItem("current_product", JSON.stringify(current_product));
                setCurrentProduct(old_current_product)
                onSetMySize(old_current_product)
                setSelectedProduct(selected_product)
                setFavoriteProductList(favorite_product)
            }
            if (selected_product) {
                setIsShowPent(true)
            }
            if (isFavoritesProduct || isCustomizeProduct || productsInformation) {
                setIsFavoritesProduct(false)
                setIsCustomizeProduct(false)
                setProductsInformation(false)
            } else {
                cleanLocalData()
                history.goBack()
            }
        }
    }

    const onSetCurrentProductSlider = () => {
        setIsShowSelectedProjectSlider(false)
        onSetMySize(currentProduct)
    }

    const onSetSelectedProductSlider = () => {
        setIsShowSelectedProjectSlider(true)
        onSetMySize(selectRecommendedProduct)
    }

    const onFindYourSize = () => {
        setIsOpenSizeModal(!isOpenSizeModal)
    }

    const onSetIsSizeSwitch = (value) => {
        setIsSizeSwitch(value)
    }

    const setIsLoadingValue = (value) => {
        setIsLoading(value)
    }
    if (!dave_user_id) {
        history.push('/')
    }

    const getSize = (value) => {
        if (value) {
            let data = []
            for (let i = 0; i < value.length; i++) {
                data[i] = { id: i, title: value[i], selected: false, key: value[i] }
                if (value.length - 1 === i) {
                    return data
                }
            }
        } else {
            return sizeSelector
        }
    }

    const onGetUpdatedData = (data) => {
        onGetLocalData()
    }

    const isDisableFindYourSize = () => {
        let data = null
        if (isShowSelectedProjectSlider) {
            if (selectRecommendedProduct?.category_name === 'Top Wear' || selectRecommendedProduct?.category_name === 'Bottom Wear') {
                data = true
            } else {
                data = false
            }
        } else {
            if (currentProduct?.category_name === 'Top Wear' || currentProduct?.category_name === 'Bottom Wear') {
                data = true
            } else {
                data = false
            }
        }
        return data
    }

    const onSubmitFilter = () => {
        const similar_products = JSON.parse(window.localStorage.getItem('similar_products'));

        if (similar_products && similar_products.data && similar_products.data.thumbnails) {
            let results = null
            if (selectStyle && selectColour && selectFit) {
                results = similar_products.data.thumbnails.filter(element => {
                    const result = element.brand_name === selectStyle && element.colour === selectColour && element.sub_category === selectFit
                    return result
                });
            } else if (selectStyle && selectColour) {
                results = similar_products.data.thumbnails.filter(element => {
                    const result = element.brand_name === selectStyle && element.colour === selectColour
                    return result
                });
            } else if (selectColour && selectFit) {
                results = similar_products.data.thumbnails.filter(element => {
                    const result = element.colour === selectColour && element.sub_category === selectFit
                    return result
                });
            } else if (selectStyle && selectFit) {
                results = similar_products.data.thumbnails.filter(element => {
                    const result = element.brand_name === selectStyle && element.sub_category === selectFit
                    return result
                });
            } else if (selectStyle) {
                results = similar_products.data.thumbnails.filter(element => {
                    const result = element.brand_name === selectStyle
                    return result
                });
            } else if (selectColour) {
                results = similar_products.data.thumbnails.filter(element => {
                    const result = element.colour === selectColour
                    return result
                });
            } else if (selectFit) {
                results = similar_products.data.thumbnails.filter(element => {
                    const result = element.sub_category === selectFit
                    return result
                });
            } else {
                results = similar_products.data.thumbnails;
            }
            console.log('results', results)
            const data = priceSorting(results)
            if (!data) {
                similarProducts.data.error = 'Product Not Found'
            } else {
                similarProducts.data.error = null
            }
            similarProducts.data.thumbnails = data
            console.log('similarProducts', similarProducts)
            setSimilarProducts(similarProducts)
            getAllSubCategoryList(similarProducts.data.thumbnails)
            setIsSetFilter(true)
        }
    }

    const priceSorting = (results) => {
        console.log('priceSorting 12', results)
        if (results.length !== 0) {
            if (selectPriceRange === 'Low To High') {
                return results.sort((a, b) => a.selling_price - b.selling_price);
            } else {
                return results.sort((a, b) => b.selling_price - a.selling_price);
            }
        }
    }

    const onClearFilter = () => {
        const similar_products = JSON.parse(window.localStorage.getItem('similar_products'));
        const current_product = JSON.parse(window.localStorage.getItem('current_product'));
        setSelectFit(null)
        setSelectColour(null)
        setSelectStyle(null)
        setSelectPriceRange('Low To High')
        setSimilarProducts(similar_products)
        getAllSubCategoryList(similar_products.data.thumbnails)
        setCurrentProduct(current_product)
        setIsSetFilter(false)
    }

    return (
        <div>
            <div className="product-screen">
                <HeaderTop
                    onProductFavoritesPress={onProductFavoritesPress}
                    renderScreen="Product"
                    onBackPress={onHeaderBackPress}
                    isFavoritesProduct={isFavoritesProduct}
                    productsInformation={productsInformation}
                    onProductsInformation={(e) => setProductsInformation(e)}
                />
                {isLoading &&
                    <div style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '100vw',
                        height: '100vh',
                        display: 'flex',
                        position: 'fixed',
                        zIndex: 9999999,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        backgroundColor: "rgba(0,0,0,0.60)"
                    }}>
                        <Loading />
                    </div>
                }
                <div style={{ height: '84vh' }}>
                    {productsInformation ?
                        <div className="products-information-view">
                            <div className="products-information-header">
                                <div className="products-information-header-title">Products information</div>
                                <button
                                    className="qr-code-close-modal"
                                    type="button" aria-label="Close" onClick={() => {
                                        setProductsInformation(false)
                                    }}>
                                    <span aria-hidden="true" style={{ color: 'red' }}>&times;</span>
                                </button>
                            </div>
                            <div className="products-image-information-view">
                                <img src={isShowSelectedProjectSlider ? selectRecommendedProduct?.image[0] : currentProduct?.image[0]} className="product-information-image" />
                                <div className="product-location-information-main-view">
                                    <div className="product-title-information">
                                        <div className="product-information-text">Floor Number :
                                            <div className="product-information-text-details"> 3</div>
                                        </div>
                                        <div className="product-information-text">Row Number :
                                            <div className="product-information-text-details"> 8</div>
                                        </div>
                                        <div className="product-information-text">Collection Area :
                                            <div className="product-information-text-details"> Men's Formal</div>
                                        </div>
                                        <div className="product-information-text">Rack Number :
                                            <div className="product-information-text-details"> 24</div>
                                        </div>
                                    </div>
                                    {/* <img src={ProductMap} className="product-map-image" /> */}
                                    <div className="product-map-image">
                                        <GoogleMapReact
                                            defaultZoom={15}
                                            defaultCenter={mapCenter}
                                        >
                                            {places.map((place) => (
                                                <Marker
                                                    key={place.id}
                                                    text={place.name}
                                                    lat={place.geometry.location.lat}
                                                    lng={place.geometry.location.lng}
                                                />
                                            ))}
                                        </GoogleMapReact>
                                    </div>
                                </div>
                            </div>
                            <div className="product-brand-name-information-view">
                                <div className="product-brand-information-view">
                                    <p className="collection-title">{isShowSelectedProjectSlider ? selectRecommendedProduct?.brand_name : currentProduct?.brand_name}</p>
                                    <p className="collection-full-title-2"
                                        style={{ fontSize: 36 }}>{isShowSelectedProjectSlider ? selectRecommendedProduct?.product_name : currentProduct?.product_name}</p>
                                </div>
                                <div className="product-name-information-view">
                                    <div
                                        className="cross-price horizontal-card-product-cross-price">{isShowSelectedProjectSlider ? selectRecommendedProduct?.price : currentProduct?.price}</div>
                                    <div className="horizontal-card-product-sell-price"
                                        style={{ fontSize: 36 }}>₹ {isShowSelectedProjectSlider ? selectRecommendedProduct?.selling_price : currentProduct?.selling_price}</div>
                                </div>
                            </div>
                            <div className="product-information-text-details"
                                style={{ marginTop: 8, fontSize: 24 }}>{isShowSelectedProjectSlider ? selectRecommendedProduct?.product_description : currentProduct?.product_description}</div>
                            <div className="product-information-text-details" style={{
                                marginTop: 15,
                                marginBottom: 15,
                                fontSize: 24,
                                color: '#000000'
                            }}>Specifications
                            </div>
                            {!currentProduct?.sleeve_length &&
                                !currentProduct?.fit &&
                                !currentProduct?.length &&
                                !currentProduct?.weave_pattern &&
                                !currentProduct?.num_pockets &&
                                !currentProduct?.pocket_type ?
                                <div />
                                :
                                <div className="product-specifications-information-main-view">
                                    {currentProduct?.sleeve_length &&
                                        <div className="product-specifications-information-view">
                                            <div className="product-information-text-details">Sleeve Length</div>
                                            <div className="product-information-text-details"
                                                style={{ marginTop: 8, fontSize: 24, }}>{currentProduct?.sleeve_length}</div>
                                        </div>}
                                    {currentProduct?.fit && <div className="product-specifications-information-view">
                                        <div className="product-information-text-details">Fit</div>
                                        <div className="product-information-text-details"
                                            style={{ marginTop: 8, fontSize: 24, }}>{currentProduct?.fit}</div>
                                    </div>}
                                    {currentProduct?.length && <div className="product-specifications-information-view">
                                        <div className="product-information-text-details">Length</div>
                                        <div className="product-information-text-details"
                                            style={{ marginTop: 8, fontSize: 24, }}>{currentProduct?.length}</div>
                                    </div>}
                                    {currentProduct?.weave_pattern &&
                                        <div className="product-specifications-information-view">
                                            <div className="product-information-text-details">Weave Pattern</div>
                                            <div className="product-information-text-details"
                                                style={{ marginTop: 8, fontSize: 24, }}>{currentProduct?.weave_pattern}</div>
                                        </div>}
                                    {currentProduct?.num_pockets &&
                                        <div className="product-specifications-information-view">
                                            <div className="product-information-text-details">Number of Pockets</div>
                                            <div className="product-information-text-details"
                                                style={{ marginTop: 8, fontSize: 24, }}>{currentProduct?.num_pockets}</div>
                                        </div>}
                                    {currentProduct?.pocket_type &&
                                        <div className="product-specifications-information-view">
                                            <div className="product-information-text-details">Pocket Type</div>
                                            <div className="product-information-text-details"
                                                style={{ marginTop: 8, fontSize: 24, }}>{currentProduct?.pocket_type}</div>
                                        </div>}
                                </div>
                            }
                            {
                                currentProduct?.sleeve_length ||
                                currentProduct?.fit ||
                                currentProduct?.length ||
                                currentProduct?.weave_pattern ||
                                currentProduct?.num_pockets ||
                                currentProduct?.pocket_type && <div className="border-bottom-class" />
                            }

                            <div className="product-specifications-information-main-view2">
                                {isShowSelectedProjectSlider ? selectRecommendedProduct?.hemline : currentProduct?.hemline && <div className="product-specifications-information-view2">
                                    <div className="product-information-text-details">Hemline</div>
                                    <div className="product-information-text-details"
                                        style={{ marginTop: 8, fontSize: 24, }}>{isShowSelectedProjectSlider ? selectRecommendedProduct?.hemline : currentProduct?.hemline}</div>
                                </div>}
                                {isShowSelectedProjectSlider ? selectRecommendedProduct?.pattern : currentProduct?.pattern && <div className="product-specifications-information-view2">
                                    <div className="product-information-text-details">Print or Pattern Type</div>
                                    <div className="product-information-text-details"
                                        style={{ marginTop: 8, fontSize: 24, }}>{isShowSelectedProjectSlider ? selectRecommendedProduct?.pattern : currentProduct?.pattern}</div>
                                </div>}
                                {isShowSelectedProjectSlider ? selectRecommendedProduct?.colour : currentProduct?.colour && <div className="product-specifications-information-view2">
                                    <div className="product-information-text-details">Colour</div>
                                    <div className="product-information-text-details"
                                        style={{ marginTop: 8, fontSize: 24, }}>{isShowSelectedProjectSlider ? selectRecommendedProduct?.colour : currentProduct?.colour}</div>
                                </div>}
                            </div>
                        </div>
                        :
                        <>
                            <div>
                                <ShareQRCodeModal
                                    isBarcodeModel={isBarcodeModel}
                                    afterOpenModal={afterOpenModal}
                                    closeModal={closeModal}
                                    setMobileNumber={setMobileNumber}
                                    onSendMobile={onSendMobile}
                                    errorsPhoneNumber={errorsPhoneNumber}
                                    productsBarcode={productsBarcode}
                                />
                            </div>
                            <div className="product-top-view">
                                <div className="row product-top-two-view">
                                    <div className="product-image-and-like-icon-view">
                                        {!isCustomizeProduct &&
                                            <div
                                                className={`${isShowPent ? "product-image-alert-view" : "product-image-alert-view-true"}`}>
                                                <div className="product-3D-modal-view">
                                                    <img src={WarningIcon} alt="warning-img" className="img-warning-icon" />
                                                    <p className="img-warning-text"> A 3D model for this product is not available.</p>
                                                </div>
                                            </div>
                                        }

                                        {/* {!isShowPent && (
                                            <div className="product-like-main-view">
                                                <div className="product-like-view" onClick={() => {
                                                    addToFavorite(currentProduct)
                                                }}>
                                                    <img src={isProductInFavorite ? LikeIcon : LikeBorderIcon}
                                                        className="product-like-icon" />
                                                </div>
                                                {!isCustomizeProduct &&
                                                    <div className="product-img-right-icon-view">
                                                        <img src={ProductHeaderIcon}
                                                            className="product-img-right-icon" />
                                                    </div>
                                                }
                                            </div>
                                        )} */}
                                        <div style={{
                                            justifyContent: !isShowPent ? 'center' : 'space-between',
                                            display: 'flex',
                                            flexDirection: 'column',
                                            maxHeight: !isCustomizeProduct ? '89%' : '100%',
                                            height: !selectedProduct && '89%',
                                        }}>
                                            {currentProduct &&
                                                <div style={{
                                                    height: isShowPent ? '45%' : '100%',
                                                    display: 'flex',
                                                    minWidth: '100%',
                                                    minHeight: 100,
                                                    alignItems: productSliderLoading && 'center',
                                                }} className={`${isShowSelectedProjectSlider ? 'product-image-border' : 'product-image-border-highlight'}`} onClick={() => {
                                                    onSetCurrentProductSlider()
                                                }}>
                                                    {productSliderLoading ?
                                                        <div style={{
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                            width: '100%',
                                                            height: '100%',
                                                            display: 'flex'
                                                        }}>
                                                            <Loading />
                                                        </div>
                                                        :
                                                        <img src={currentProduct.image ? currentProduct?.image[0] : ''} className="product-title-image" />
                                                    }
                                                    {/* <img src={CustomizProductImage} className="product-title-image" /> */}
                                                </div>
                                            }
                                            {selectedProduct &&
                                                <div style={{
                                                    height: isShowPent ? '45%' : '100%',
                                                    marginTop: 20,
                                                    display: 'flex',
                                                    minWidth: '100%',
                                                    minHeight: 100
                                                }} className={`${isShowSelectedProjectSlider ? 'product-image-border-highlight' : 'product-image-border'}`} onClick={() => {
                                                    onSetSelectedProductSlider()
                                                }}>
                                                    <img src={selectedProduct} className="product-title-image" />
                                                    {/* <img src={CustomizProductImage} className="product-title-image" /> */}
                                                </div>
                                            }
                                        </div>
                                    </div>
                                    <div className="product-detail-main-view"
                                        style={{ display: isCustomizeProduct && 'flex' }}>
                                        <div
                                            style={{ backgroundColor: !isCustomizeProduct && '#fff', borderRadius: 10, }}>
                                            <div style={{ paddingTop: 10, }}>
                                                <p className="collection-title">{isShowSelectedProjectSlider ? selectRecommendedProduct?.brand_name : currentProduct?.brand_name}</p>
                                                <p className="collection-title-2">{isShowSelectedProjectSlider ? selectRecommendedProduct?.product_name : currentProduct?.product_name}</p>
                                                {!isCustomizeProduct &&
                                                    productSliderLoading ?
                                                    <div className="product-slider-image" style={{
                                                        display: 'flex',
                                                        alignItems: 'center',
                                                        justifyContent: 'center',
                                                        paddingTop: 5,
                                                        paddingBottom: 5
                                                    }}>
                                                        <Loading />
                                                    </div>
                                                    :
                                                    <Carousel
                                                        style={{ paddingTop: 10 }}
                                                        ref={productCarouselRef}
                                                        enableAutoPlay
                                                        showArrows={false}
                                                        itemPadding={[0, 5]}
                                                        outerSpacing={10}
                                                        autoPlaySpeed={3000}
                                                        itemsToShow={itemsPerPage}
                                                        onNextEnd={({ index }) => {
                                                            clearTimeout(resetTimeout)
                                                            if (index === totalPages) {
                                                                var path = window.location.pathname;
                                                                var page = path.split("/").pop();
                                                                if (page === 'product' && productCarouselRef?.current?.goTo) {
                                                                    resetTimeout = setTimeout(() => {
                                                                        if (page === 'product' && productCarouselRef?.current?.goTo) {
                                                                            productCarouselRef.current.goTo(0)
                                                                        }
                                                                    }, 1500) // same time
                                                                }
                                                            }
                                                        }}
                                                        renderPagination={({ pages, activePage, onClick }) => {
                                                            return (
                                                                <div style={{
                                                                    flexDirection: 'row',
                                                                    display: 'flex',
                                                                    height: 0,
                                                                    width: 0,
                                                                    alignItems: 'center'
                                                                }} />
                                                            )
                                                        }}
                                                    >
                                                        {isShowSelectedProjectSlider && recommendedProductSlider.map((item, index) => {
                                                            return (
                                                                item !== 'None' && item &&
                                                                <div className="product-slider-image" key={index}>
                                                                    <img
                                                                        className="product-slider-img"
                                                                        src={item}
                                                                        style={{
                                                                            height: '100%',
                                                                            width: isShowSelectedProjectSlider && recommendedProductSlider.length <= 1 ? '50%' : '100%',
                                                                        }}
                                                                    />
                                                                </div>
                                                            )
                                                        })}
                                                        {!isShowSelectedProjectSlider && productSlider?.map((item, index) => {
                                                            return (
                                                                item !== 'None' && item &&
                                                                <div className="product-slider-image" key={index}>
                                                                    <img
                                                                        className="product-slider-img"
                                                                        src={item}
                                                                        style={{
                                                                            height: '100%',
                                                                            width: !isShowSelectedProjectSlider && productSlider.length <= 1 ? '50%' : '100%',
                                                                        }}
                                                                    />
                                                                </div>
                                                            )
                                                        })}
                                                    </Carousel>
                                                }
                                            </div>
                                            <div
                                                style={{ padding: 10, paddingBottom: 20, marginBottom: 15 }}>
                                                <div style={{ justifyContent: 'space-between', display: 'flex' }}>
                                                    <div style={{
                                                        justifyContent: 'flex-start',
                                                        alignItems: 'center',
                                                        display: 'flex',
                                                    }} className="cross-price">₹ {isShowSelectedProjectSlider ? selectRecommendedProduct?.price : currentProduct?.price}</div>
                                                    <div className="real-price">
                                                        ₹ {isShowSelectedProjectSlider ? selectRecommendedProduct?.selling_price : currentProduct?.selling_price}
                                                    </div>
                                                    <div style={{
                                                        display: 'flex',
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between'
                                                    }}>
                                                        <div style={{
                                                            display: 'flex',
                                                            flexDirection: 'row',
                                                            marginTop: isShowSelectedProjectSlider ? selectRecommendedProduct?.productPrice : currentProduct?.productPrice ? -10 : 0
                                                        }}>
                                                            {!isCustomizeProduct &&
                                                                <div className="like-product-view" onClick={() => {
                                                                    addToFavorite(isShowSelectedProjectSlider ? selectRecommendedProduct : currentProduct)
                                                                }}>
                                                                    <img src={isProductInFavorite ? LikeIcon : LikeBorderIcon} className="like-product-icon" />
                                                                </div>
                                                            }
                                                            {/* <div className="share-product-view" onClick={() => {
                                                            openModal()
                                                        }}>
                                                            <img
                                                                src={ShareIcon}
                                                                className="share-product-icon"
                                                            />
                                                        </div> */}
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* <div className="availability-price-title">
                                                    Availability : 2 Piece
                                                </div> */}
                                                {!isCustomizeProduct &&
                                                    <>
                                                        <div className="drop-down-main-view">
                                                            <div style={{
                                                                backgroundColor: '#fff',
                                                                width: '49%',
                                                                height: '90%',
                                                                borderRadius: 10
                                                            }}>
                                                                <DropdownSelect
                                                                    selectorTitle="Choose Size"
                                                                    selectorData={getSize(isShowSelectedProjectSlider ? selectRecommendedProduct?.sizes : currentProduct?.sizes)}
                                                                    selectorValue={selectYourSize}
                                                                    onChangeSelectorValue={(e) => handleChangeSize(e)}
                                                                />
                                                            </div>
                                                            <div style={{
                                                                backgroundColor: isDisableFindYourSize() ? '#fff' : '#c2c2c2',
                                                                width: '49%',
                                                                height: '90%',
                                                                borderRadius: 10,
                                                                display: 'flex',
                                                                justifyContent: 'center',
                                                                alignItems: 'center',
                                                                cursor: 'pointer'
                                                            }} onClick={() => isDisableFindYourSize() ? onFindYourSize() : console.log('btn is disable')}>
                                                                <img src={FindYourSize} className="find-your-size-logo" />
                                                                <div className="find-your-size-title">Find your size
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="product-information-btn-main-view">
                                                            <div style={{
                                                                backgroundColor: '#091398',
                                                                width: '100%',
                                                                height: '100%',
                                                                borderRadius: 10,
                                                                display: 'flex',
                                                                justifyContent: 'center',
                                                                alignItems: 'center',
                                                                cursor: 'pointer'
                                                            }} onClick={() => {
                                                                setProductsInformation(true)
                                                            }}>
                                                                <img src={ViewInformation} className="view-information-logo" />
                                                                <div className="information-title">View Information / Locate
                                                                </div>
                                                            </div>
                                                            {/* <div style={{
                                                                backgroundColor: '#091398',
                                                                width: '49%',
                                                                height: '100%',
                                                                borderRadius: 10,
                                                                display: 'flex',
                                                                justifyContent: 'center',
                                                                alignItems: 'center',
                                                                cursor: 'pointer'
                                                            }} onClick={() => {
                                                                setIsCustomizeProduct(true)
                                                            }}>
                                                                <img src={CustomizeIcon} className="customize-logo" />
                                                                <div className="information-title">Customize</div>
                                                            </div> */}
                                                        </div>
                                                    </>
                                                }
                                            </div>
                                        </div>
                                        {isCustomizeProduct &&
                                            <div className="product-category-main-view">
                                                <div className="product-category-title">Category</div>
                                                <div className="product-search-main-view">
                                                    <input className="search-product-input" type="text"
                                                        placeholder="Search..."
                                                        value={productCategory}
                                                        onChange={(e) => setProductCategory(e.target.value)}
                                                        style={{ border: 'none' }} />
                                                    <img
                                                        className="set-icon send-input-btn"
                                                        src={IconSend}
                                                        aria-hidden="true"
                                                        onClick={() => {
                                                            setSelectedProductCategory(productCategory)
                                                        }}
                                                    />
                                                </div>
                                                <div className="product-category-list-main-view">
                                                    {productCategoryList.map((item, index) => {
                                                        return (
                                                            <div key={index}
                                                                className={`${selectedProductCategory === item.title ? 'selected-product-category-card' : 'product-category-card'}`}
                                                                onClick={() => {
                                                                    setSelectedProductCategory(item.title)
                                                                }}>
                                                                {item.title}
                                                            </div>
                                                        )
                                                    })}
                                                </div>
                                            </div>
                                        }
                                        {!isCustomizeProduct &&
                                            <div style={{ backgroundColor: '#fff', borderRadius: 10, marginTop: 15 }}>
                                                <div style={{ padding: 15 }}>
                                                    <div style={{
                                                        backgroundColor: '#fff',
                                                        width: '100%',
                                                        borderRadius: 10,
                                                        marginBottom: 5,
                                                        justifyContent: 'space-around',
                                                        alignItems: 'center',
                                                        alignSelf: 'center',
                                                        flexDirection: 'row',
                                                        display: 'flex'
                                                    }}>
                                                        <div style={{ width: '49%' }}>
                                                            <div
                                                                className="d-flex align-items-center align-self-center basic-information-input-title-view">
                                                                <label
                                                                    htmlFor="price-range"
                                                                    className="basic-information-input-title">
                                                                    Price Range
                                                                </label>
                                                            </div>
                                                            <DropdownSelect
                                                                selectorTitle="Select Price Range"
                                                                selectorData={PriceRangeSelector}
                                                                selectorValue={selectPriceRange}
                                                                onChangeSelectorValue={(e) => handleChangePriceRange(e)}
                                                            />
                                                        </div>
                                                        <div style={{ width: '49%' }}>
                                                            <div
                                                                className="d-flex align-items-center align-self-center basic-information-input-title-view">
                                                                <label htmlFor="colour"
                                                                    className="basic-information-input-title">Colour</label>
                                                            </div>
                                                            <DropdownSelect
                                                                selectorTitle="Select Colour"
                                                                selectorData={allColourList}
                                                                selectorValue={selectColour}
                                                                onChangeSelectorValue={(e) => handleChangeColour(e)}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div style={{
                                                        backgroundColor: '#fff',
                                                        width: '100%',
                                                        borderRadius: 10,
                                                        marginTop: 5,
                                                        marginBottom: 5,
                                                        justifyContent: 'space-around',
                                                        alignItems: 'center',
                                                        alignSelf: 'center',
                                                        flexDirection: 'row',
                                                        display: 'flex'
                                                    }}>
                                                        <div style={{ width: '49%' }}>
                                                            <div
                                                                className="d-flex align-items-center align-self-center basic-information-input-title-view">
                                                                <label htmlFor="Brands"
                                                                    className="basic-information-input-title">Brands</label>
                                                            </div>
                                                            <DropdownSelect
                                                                selectorTitle="Select Brands"
                                                                selectorData={allBrandNameList}
                                                                selectorValue={selectStyle}
                                                                onChangeSelectorValue={(e) => handleChangeStyle(e)}
                                                            />
                                                        </div>
                                                        <div style={{ width: '49%', }}>
                                                            <div
                                                                className="d-flex align-items-center align-self-center basic-information-input-title-view">
                                                                <label htmlFor="Collection"
                                                                    className="basic-information-input-title">Sub Category</label>
                                                            </div>
                                                            <DropdownSelect
                                                                selectorTitle="Select Sub Category"
                                                                selectorData={allSubCategoryList}
                                                                selectorValue={selectFit}
                                                                onChangeSelectorValue={(e) => handleChangeFit(e)}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        }
                                        <div className="submit-filter-btn-view">
                                            <div style={{
                                                backgroundColor: '#091398',
                                                width: isSetFilter ? '49%' : '100%',
                                                height: 70,
                                                marginTop: 15,
                                                borderRadius: 10,
                                                display: 'flex',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                cursor: 'pointer'
                                            }} onClick={() => {
                                                // onChangeFilter()
                                                onSubmitFilter()
                                            }}>
                                                <div className="information-title">Submit Filter</div>
                                            </div>
                                            {isSetFilter &&
                                                <div style={{
                                                    backgroundColor: '#091398',
                                                    width: '49%',
                                                    height: 70,
                                                    marginTop: 15,
                                                    borderRadius: 10,
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    cursor: 'pointer'
                                                }} onClick={() => {
                                                    onClearFilter()
                                                }}>
                                                    <div className="information-title">Clear Filter</div>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    }

                    <div style={{ height: '34vh', width: '100%', overflow: 'auto' }} onScroll={handleScroll}>
                        <div style={{ padding: 5, }}>
                            <div style={{ paddingTop: 8 }}>
                                <div className="border-bottom-class" />
                                {!isFavoritesProduct && isCustomizeProduct &&
                                    <div className="show-selected-product-category-view">
                                        <div className="selected-product-category-title-view">
                                            <div className="show-related-products-title"
                                                style={{ marginTop: 0 }}>{selectedProductCategory}</div>
                                            <div className="clear-style-products-title">Clear Style</div>
                                        </div>
                                        <div style={{ height: '100%', width: '100%', marginTop: 15 }}>
                                            <ProductCardSwiper onHandlePress={handleScroll}
                                                productCardListData={productCardList} />
                                        </div>
                                    </div>
                                }

                                {isFavoritesProduct &&
                                    <>
                                        <div className="selected-product-category-title-view">
                                            <div className="show-related-products-title"
                                                style={{ marginTop: 0 }}>Favorites Product
                                            </div>
                                            {/* <div className="share-product-view" onClick={() => {
                                            }}>
                                                <img
                                                    src={ShareIcon}
                                                    className="share-product-icon"
                                                />
                                            </div> */}
                                        </div>
                                        {favoriteProductList.length !== 0 && (
                                            <div style={{ marginTop: 30, marginBottom: 20, }}>
                                                <Carousel
                                                    ref={carouselRef2}
                                                    itemsToShow={3}
                                                    renderArrow={myArrow}
                                                    renderPagination={({ pages, activePage, onClick }) => {
                                                        return (
                                                            <div style={{
                                                                flexDirection: 'row',
                                                                display: 'flex',
                                                                height: 0,
                                                                width: 0,
                                                                alignItems: 'center'
                                                            }} />
                                                        )
                                                    }}
                                                >
                                                    {favoriteProductList?.map((item, index) => {
                                                        return (
                                                            <div key={index} className={`${currentProduct.product_code === item.product_code ? 'horizontal-product-card-box-shadow' : 'horizontal-product-card'}`}
                                                                onClick={() => {
                                                                    onSelectedSimilarProducts(index, item)
                                                                }}>
                                                                <div style={{ width: '40%', height: '100%' }}>
                                                                    <img src={item?.image && item?.image[0]} className="product-card-image" />
                                                                </div>
                                                                <div style={{ width: '55%', padding: 5 }}>
                                                                    <div
                                                                        className="horizontal-card-product-title">{item.product_name}</div>
                                                                    {item.productSize && <div
                                                                        className="horizontal-card-product-size">Size: {item.productSize}</div>}
                                                                    {item.price && <div
                                                                        className="cross-price horizontal-card-product-cross-price">₹ {item.price}</div>}
                                                                    <div
                                                                        className="horizontal-card-product-sell-price">₹ {item.selling_price}</div>
                                                                    {/* <div className="horizontal-card-product-share-icon-view"
                                                                    onClick={() => {
                                                                        openModal(item.barcode)
                                                                    }}>
                                                                    <img
                                                                        src={ShareIcon}
                                                                        className="horizontal-card-product-share-icon"
                                                                    />
                                                                </div> */}
                                                                </div>
                                                            </div>
                                                        )
                                                    })}
                                                </Carousel>
                                            </div>
                                        )}
                                        {favoriteProductList.length === 0 &&
                                            <div className="show-related-products-not-found-title">Favorites Products not found</div>
                                        }
                                    </>
                                }

                                {!isFavoritesProduct && !isCustomizeProduct && (
                                    <>
                                        <div className="show-related-products-title">{similarProducts && similarProducts.placeholder}</div>
                                        <div style={{ marginTop: 15, marginBottom: 20, }}>
                                            {isSimilarProductsLoading &&
                                                <div style={{
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    height: 217,
                                                    display: 'flex'
                                                }}>
                                                    <Loading />
                                                </div>
                                            }
                                            {!isSimilarProductsLoading && similarProducts.data && similarProducts.data.thumbnails && similarProducts.data.thumbnails.length !== 0 &&
                                                <Carousel
                                                    ref={carouselRef2}
                                                    itemsToShow={3}
                                                    renderArrow={myArrow}
                                                    renderPagination={({ pages, activePage, onClick }) => {
                                                        return (
                                                            <div style={{
                                                                flexDirection: 'row',
                                                                display: 'flex',
                                                                height: 0,
                                                                width: 0,
                                                                alignItems: 'center'
                                                            }} />
                                                        )
                                                    }}
                                                >
                                                    {similarProducts?.data?.thumbnails?.map((item, index) => {
                                                        return (
                                                            <div key={index} className="horizontal-product-card"
                                                                onClick={() => {
                                                                    onSelectedSimilarProducts(index, item)
                                                                }}>
                                                                <div style={{ width: '40%', height: '100%' }}>
                                                                    <img src={item?.image && item?.image[0]} className="product-card-image" />
                                                                </div>
                                                                <div style={{ width: '55%', padding: 5 }}>
                                                                    <div
                                                                        className="horizontal-card-product-title">{item.title ? item.title : item.product_name}</div>
                                                                    {item.productSize && <div
                                                                        className="horizontal-card-product-size">Size: {item.productSize}</div>}
                                                                    {item.price && <div
                                                                        className="cross-price horizontal-card-product-cross-price">₹ {item.price}</div>}
                                                                    <div
                                                                        className="horizontal-card-product-sell-price">₹ {item.selling_price}</div>
                                                                    {/* <div className="horizontal-card-product-share-icon-view"
                                                                    onClick={() => {
                                                                        openModal(item.barcode)
                                                                    }}>
                                                                    <img
                                                                        src={ShareIcon}
                                                                        className="horizontal-card-product-share-icon"
                                                                    />
                                                                </div> */}
                                                                </div>
                                                            </div>
                                                        )
                                                    })}
                                                </Carousel>
                                            }
                                            {!isSimilarProductsLoading && similarProducts.data.error &&
                                                <div className="show-related-products-not-found-title">{similarProducts.data.error}</div>
                                            }
                                        </div>
                                        <div className="border-bottom-class" />
                                    </>
                                )}
                                {!isFavoritesProduct && !isCustomizeProduct && (
                                    <div>
                                        <div className="show-related-products-title">Recommended Look
                                        </div>
                                        <div style={{ marginTop: 15, marginBottom: 15, }}>
                                            {isRecommendedLookProductsLoading &&
                                                <div style={{
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    height: 217,
                                                    display: 'flex'
                                                }}>
                                                    <Loading />
                                                </div>
                                            }
                                            {!isRecommendedLookProductsLoading && recommendedLookProducts.data && recommendedLookProducts.data.thumbnails && recommendedLookProducts.data.thumbnails.length !== 0 &&
                                                <Carousel
                                                    ref={carouselRef3}
                                                    itemsToShow={3}
                                                    renderArrow={myArrow}
                                                    renderPagination={({ pages, activePage, onClick }) => {
                                                        return (
                                                            <div style={{
                                                                flexDirection: 'row',
                                                                display: 'flex',
                                                                height: 0,
                                                                width: 0,
                                                                alignItems: 'center'
                                                            }} />
                                                        )
                                                    }}
                                                >
                                                    {recommendedLookProducts?.data?.thumbnails?.map((item, index) => {
                                                        return (
                                                            <div key={index} className={`${selectRecommendedProduct.product_code === item.product_code ? 'horizontal-product-card-box-shadow' : 'horizontal-product-card'}`}
                                                                onClick={() => {
                                                                    onSelectPent(item)
                                                                }}>
                                                                <div style={{ width: '40%', height: '100%' }}>
                                                                    <img src={item?.image && item?.image[0]} className="product-card-image" />
                                                                </div>
                                                                <div style={{ width: '55%', padding: 5 }}>
                                                                    <div
                                                                        className="horizontal-card-product-title">{item.product_name}</div>
                                                                    {item.productSize && <div
                                                                        className="horizontal-card-product-size">Size: {item.sizes}</div>}
                                                                    {item.price && <div
                                                                        className="cross-price horizontal-card-product-cross-price">₹ {item.price}</div>}
                                                                    <div
                                                                        className="horizontal-card-product-sell-price">₹ {item.selling_price}</div>
                                                                    {/* <div className="horizontal-card-product-share-icon-view"
                                                                    onClick={() => {
                                                                        openModal()
                                                                    }}>
                                                                    <img
                                                                        src={ShareIcon}
                                                                        className="horizontal-card-product-share-icon"
                                                                    />
                                                                </div> */}
                                                                </div>
                                                            </div>
                                                        )
                                                    })}
                                                </Carousel>
                                            }
                                            {!isRecommendedLookProductsLoading && recommendedLookProducts.data.error &&
                                                <div className="show-related-products-not-found-title">{recommendedLookProducts.data.error}</div>
                                            }
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    isOpen={isOpenSizeModal}
                    onRequestClose={onFindYourSize}
                    contentLabel="Example Modal"
                    style={customBlurModalStyles}
                >
                    <FindYourFitModal
                        onSetIsSizeSwitch={(e) => onSetIsSizeSwitch(e)}
                        isSizeSwitch={isSizeSwitch}
                        onFindYourSize={onFindYourSize}
                        stepNumber={1}
                        isGoBack={true}
                        isShowGenderSelection={false}
                        setIsLoadingValue={setIsLoadingValue}
                    />
                </Modal>
                <div>
                    <div style={{ position: 'fixed', left: 0, right: 0, bottom: 0 }}>
                        {isBottomBarLoading ?
                            <div style={{ height: 217, justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                                <Loading />
                            </div>
                            :
                            <BottomTabBar
                                screen="Profile"
                                discountOptions={discountOptions}
                                handleClickDiscount={(e) => handleClickDiscount(e)}
                                onManageLoading={(e) => setIsLoadingValue(e)}
                                onGetUpdatedData={(e) => onGetUpdatedData(e)}
                            />
                        }
                    </div>
                </div>
            </div>
        </div >
    );
}


export default Product;
