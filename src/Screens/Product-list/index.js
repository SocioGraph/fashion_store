import React, { useEffect, useState } from 'react';
import { useAlert } from 'react-alert';
import { confirmAlert } from 'react-confirm-alert';
import HeaderTop from '../../Components/HeaderTop';
import Loading from '../../Components/Loading';
import history from '../../history';
import { apiActions } from '../../server';
import './product-list.css';

function ProductList(props) {
    const [productListData, setProductListData] = useState([]);
    const [isDataLoading, setIsDataLoading] = useState(true);
    const [timeLeft, setTimeLeft] = useState(100);
    const [timeLogoutLeft, setTimeLogoutLeft] = useState(null);

    const handleScroll = () => {
        setTimeLeft(100)
        setTimeLogoutLeft(null)
    };

    useEffect(() => {
        const setFromEvent = () => {
            setTimeLeft(100)
            setTimeLogoutLeft(null)
        }
        window.addEventListener("mousemove", setFromEvent);
    }, []);

    useEffect(() => {
        const data = props.location.state.data;
        setProductListData(data ? data : [])
        setIsDataLoading(false)
    }, [])

    useEffect(() => {
        if (!timeLeft) {
            if (timeLogoutLeft === null) {
                setTimeLogoutLeft(10)
            }
            return confirmAlert({
                customUI: ({ onClose }) => {
                    if (timeLogoutLeft === 0) {
                        onClose()
                        localStorage.clear()
                        apiActions.clearCookies()
                        cleanLocalData()
                        history.push({
                            pathname: '/',
                            state: {
                                isSignup: true,
                            }
                        });
                    }
                    return (
                        <div className='custom-ui'>
                            <div>
                                <p className="react-confirm-alert-title">Are you still around?</p>
                                <p className="react-confirm-alert-message">Closing Session in <span
                                    className="react-confirm-alert-timer">{timeLogoutLeft} Sec</span></p>
                            </div>
                            <div className="react-confirm-alert-button">
                                {/* <button className="react-confirm-alert-no-button" onClick={() => { onNoButtonPress() }}>No</button> */}
                                <button className="react-confirm-alert-yes-button" onClick={() => {
                                    onClose()
                                }}>Yes, I am still here.
                                </button>
                            </div>
                        </div>
                    );
                }
            });

        }

        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 1);
        }, 1000);

        return () => clearInterval(intervalId);
    }, [timeLeft, timeLogoutLeft]);

    useEffect(() => {
        if (!timeLogoutLeft) return;
        const intervalId = setInterval(() => {
            setTimeLogoutLeft(timeLogoutLeft - 1);
        }, 1000);
        return () => clearInterval(intervalId);
    }, [timeLogoutLeft]);

    const cleanLocalData = () => {
        window.localStorage.removeItem('recommended_products');
        window.localStorage.removeItem('current_product');
        window.localStorage.removeItem('similar_products');
    }

    const alert = useAlert();
    const dave_engagement_id = window.localStorage.getItem('dave_engagement_id');
    const dave_user_id = window.localStorage.getItem('dave_user_id');

    const onGetProduct = (product_code) => {
        setIsDataLoading(true)
        apiActions.ajaxRequestWithData(`/object/product/${product_code}`, "GET", {},
            (res) => {
                window.localStorage.setItem("gender", res?.genders[0]);
                const objSize = Object.keys(res).length
                if (objSize !== 0) {
                    window.localStorage.setItem("current_product", JSON.stringify(res));
                    apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
                        "engagement_id": dave_engagement_id,
                        "refresh_cache": true,
                        "system_response": "sr_init",
                        "customer_response": product_code,
                        "customer_state": "cs_similar_product"
                    },
                        (response) => {
                            if (response?.data?.thumbnails) {
                                window.localStorage.setItem("similar_products", JSON.stringify(response));
                                apiActions.ajaxRequestWithData(`/conversation/deployment_web/${dave_user_id}`, "POST", {
                                    "engagement_id": dave_engagement_id,
                                    "refresh_cache": true,
                                    "system_response": "sr_init",
                                    "customer_response": product_code,
                                    "customer_state": "cs_active_nudge_recommendations"
                                },
                                    (resp) => {
                                        if (resp.data.thumbnails) {
                                            window.localStorage.setItem("recommended_products", JSON.stringify(resp));
                                            history.push({
                                                pathname: '/product',
                                            });
                                        }
                                        if (resp?.data?.error) {
                                            alert.error(response?.data?.error);
                                        }

                                    },
                                    (err) => {
                                        alert.error(err?.responseJSON?.error);
                                    }
                                )
                            }
                            if (response?.data?.error) {
                                alert.error(response?.data?.error);
                            }
                        },
                        (err) => {
                            alert.error(err?.responseJSON?.error);
                        }
                    )
                } else {
                    alert.error("Product not found!");
                    setIsDataLoading(false)
                }
            },
            (error) => {
                alert.error(error.responseJSON.error);
                setIsDataLoading(false)
            }
        );
    }

    if (!dave_user_id) {
        history.push('/')
    }

    return (
        <div className="product-screen" onScroll={handleScroll}>
            <HeaderTop renderScreen="Product-list" />
            {isDataLoading ? (
                <div className="loading-home-screen-view">
                    <Loading />
                </div>
            ) : <></>}
            <div className="product-list-view">
                <div className="row product-list-card">
                    {productListData?.map((item, index) => {
                        return (
                            <div key={index} className="horizontal-product-card product-list-card-view" onClick={() => { onGetProduct(item.product_code) }}>
                                <div className="product-card-image-view">
                                    <img src={item?.image[0]} className="product-card-image" />
                                </div>
                                <div className="product-card-details-view">
                                    <div className="horizontal-card-product-title">{item.product_name}</div>
                                    <div
                                        className="horizontal-card-product-size">Size: {item.productSize}</div>
                                    <div
                                        className="cross-price horizontal-card-product-cross-price">₹ {item.price}</div>
                                    <div className="horizontal-card-product-sell-price">₹ {item.selling_price}</div>
                                </div>
                            </div>
                        )
                    })}
                </div>

                <div className={`${productListData.length === 0 && 'product-not-found-main-view'}`}>
                    {productListData?.length === 0 &&
                        <div className="product-not-found-view">
                            <h1 className="product-not-found-text">  Product not found</h1>
                        </div>
                    }
                </div>
            </div>
        </div>
    )
}

export default ProductList
