import BlueIconArrowLeft from './img/blue-icon-arrow-left.png';
import BlueIconArrowRight from './img/blue-icon-arrow-right.png';
import CloseIcon from './img/close.png';
import AppLogoHeader from './img/daveai_logo_header.png';
import FindYourSize from './img/find_your_size.png';
import IconArrowLeft from './img/icon-arrow-left.png';
import IconArrowRight from './img/icon-arrow-right.png';
import IconBack from './img/icon-back.png';
import IconClose from './img/icon-close.png';
import IconHome from './img/icon-home.png';
import IconMicrophone from './img/icon-microphone.png';
import IconSend from './img/icon-send.png';
import IconStopSquared from './img/icon-stop-squared.png';
import LikeIcon from './img/like_icon.png';
import LikeBorderIcon from './img/like_icon_border.png';
import p1 from './img/p1.png';
import p2 from './img/p2.png';
import p3 from './img/p3.png';
import p4 from './img/p4.png';
import SwipeArrow from './img/right_arrow.svg';
import ScannerLogo from './img/scanner.png';
import ViewInformation from './img/view_information.png';
import WarningIcon from './img/warning.svg';
import IconUpArrow from './img/icon_up_arrow.png';
import IconDownArrow from './img/icon_down_arrow.png';

const Images = {
  IconArrowRight,
  IconArrowLeft,
  BlueIconArrowRight,
  BlueIconArrowLeft,
  IconMicrophone,
  IconStopSquared,
  IconSend,
  IconBack,
  IconClose,
  CloseIcon,
  AppLogoHeader,
  IconHome,
  LikeIcon,
  SwipeArrow,
  p1,
  p2,
  p3,
  p4,
  FindYourSize,
  ScannerLogo,
  LikeBorderIcon,
  ViewInformation,
  WarningIcon,
  IconUpArrow,
  IconDownArrow,
}

export default Images;